import ast
import io
import re
import os

from glob import glob
from os.path import basename
from os.path import dirname
from os.path import join
from os.path import splitext

from setuptools import setup
from setuptools import find_packages

current_directory = os.path.abspath(os.path.dirname(__file__))


def read(*names, **kwargs):
    return io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ).read()


_version_re = re.compile(r'__version__\s+=\s+(.*)')
with open('laser/__init__.py', 'rb') as f:
    __version__ = str(ast.literal_eval(_version_re.search(f.read().decode('utf-8')).group(1)))


setup(
    name='laser',
    version=__version__,
    license='Copyright (C) Raymetrics S.A. - All Rights Reserved '
            'Unauthorized copying of this file, via any medium is strictly prohibited '
            'Proprietary and confidential '
            'Written by Epameinondas Kiriakopoulos <ekiriakopoulos@raymetrics.com>, May 2018',
    description='Python library for communication with laser',
    long_description='%s\n%s' % (
        re.compile('^.. start-badges.*^.. end-badges', re.M | re.S).sub('', read('README.md')),
        re.sub(':[a-z]+:`~?(.*?)`', r'``\1``', read('CHANGELOG.md'))
    ),
    author='Epameinondas Kiriakopoulos',
    author_email='ekiriakopoulos@raymetrics.com',
    url='https://gitlab.com/raymetrics/Laser',
    packages=find_packages(),
    package_dir={'': '.'},
    py_modules=[splitext(basename(path))[0] for path in glob('./*.py')],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        # complete classifier list: http://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3.6',
        'Topic :: Laser Utilities'
    ],
    keywords=[
        'raymetrics', 'laser', 'commands', 'tests'
    ],
    install_requires=[
        'pyserial==3.4', 'argparse==1.4.0', 'pytest==3.4.1', 'pytest-cov==2.5.1'
    ],
    extras_require={
        'rst': ['Sphinx>=1.6.6'],
        ':python_version=="3.6"': ['argparse==1.4.0', 'pytest==3.4.1', 'pytest-cov==2.5.1', 'mock==2.0.0'],

    },
    entry_points={
        'console_scripts': [
            'laser = laser.main:main',
            'laser_test = laser.tests.main:main'
        ]
    }
)
