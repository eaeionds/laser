import logging

from laser.laser_main.interface.argparse_controller import ArgparseController
from laser.laser_main.interface.ui_controller import UIController

logging.basicConfig(
    format='[%(name)-45s]: %(message)s',
    datefmt='%F %T',
    level=logging.DEBUG
)

logger = logging.getLogger(__name__)


def main():
    args = UIController().parser.parse_args()
    argparse_controller = ArgparseController(args)
    argparse_controller.run()


if __name__ == "__main__":
    main()
