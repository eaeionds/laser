import pytest

from laser.laser_main.laser_instance import LaserVersion322


def pytest_addoption(parser):
    parser.addoption('--additional_arguments', action="store", default='')


@pytest.fixture
def laser_instance(request):
    version, port = request.config.getoption("--additional_arguments").split('_')
    if version == '322':
        return LaserVersion322(port=port)
    else:
        raise Exception("Version not supported")
