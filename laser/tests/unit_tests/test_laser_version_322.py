import logging

from laser.laser_base.response import Response

logger = logging.getLogger(__name__)


class TestLaserVersion322(object):

    # ---------------------------------------- TEST COMMANDS ----------------------------------------

    @staticmethod
    def test_set_q_switch_delay(laser_instance):
        response = laser_instance.commands().set_q_switch_delay(delay=200)
        assert response == 'W200'
        logger.debug(' Method ""set_q_switch_delay"" returns: {}'.format(response))

    @staticmethod
    def test_set_flashlamp_frequency(laser_instance):
        response = laser_instance.commands().set_flashlamp_frequency(frequency=3000)
        assert response == 'D3000'
        logger.debug(' Method ""set_q_switch_delay"" returns: {}'.format(response))

    # ---------------------------------------- TEST VALIDATORS -----------------------------------------

    # ---------------------------------------- test valid cases ----------------------------------------

    @staticmethod
    def test_interlock_fault_validator(laser_instance):
        response_argument = {
            'type': 'laser', 'id': 'status_get_interlock_fault_byte_one',
            'response': {'e_stop_button': '0', 'bnc_interlock': '0', 'laser_head_thermostat': '1',
                         'laser_head_housing_switch': '1', 'ice450_housing_switch': '1', 'internal_bus_error': '0',
                         'external_bus_error': '0', 'flashlamp_timeout': '0'}, 'error_message': None}
        response = laser_instance.validator().interlock_fault_validator(response_argument)

        assert response == {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_interlock_fault_byte_one',
            'response': {'e_stop_button': '0', 'bnc_interlock': '0', 'laser_head_thermostat': '1',
                         'laser_head_housing_switch': '1', 'ice450_housing_switch': '1', 'internal_bus_error': '0',
                         'external_bus_error': '0', 'flashlamp_timeout': '0'}
        }

    @staticmethod
    def test_status_word_validator(laser_instance):
        response_argument = {
            'type': 'laser',
            'id': 'status_word',
            'response': {'interlocks': '1', 'simmer': '0', 'flashlamp_sync_mode': '0', 'q_switch_sync_mode': '0'},
            'error_message': None
        }
        response = laser_instance.validator().status_word_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': None,
            'id': 'status_word',
            'response': {'interlocks': '1', 'simmer': '0', 'flashlamp_sync_mode': '0', 'q_switch_sync_mode': '0'}}

    @staticmethod
    def test_status_q_switch_delay_validator(laser_instance):
        response_argument = {'type': 'laser', 'id': 'status_q_switch_delay', 'response': {'q_switch_delay': 999},
                             'error_message': None}
        response = laser_instance.validator().status_q_switch_delay_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': None,
            'id': 'status_q_switch_delay',
            'response': {'q_switch_delay': 999}
        }

    @staticmethod
    def test_validate_status_get_flashlamp_frequency(laser_instance):
        response_argument = {
            'type': 'laser', 'id': 'status_get_flashlamp_frequency', 'response': {'frequency': 100.00},
            'error_message': None}
        response = laser_instance.validator().flashlamp_frequency_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_flashlamp_frequency',
            'response': {'frequency': 100.0}}

    # ---------------------------------------- test invalid cases ----------------------------------------

    @staticmethod
    def test_interlock_fault_validator_invalid(laser_instance):
        response_argument = {
            'type': 'laser', 'id': 'status_get_interlock_fault_byte_one',
            'response': {'e_stop_button': '2', 'bnc_interlock': '0', 'laser_head_thermostat': '1',
                         'laser_head_housing_switch': '1', 'ice450_housing_switch': '1', 'internal_bus_error': '0',
                         'external_bus_error': '0', 'flashlamp_timeout': '0'}, 'error_message': None}
        response = laser_instance.validator.interlock_fault_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': "Validator error, value outside valid response limits 0-1: [{'e_stop_button': '2'}]",
            'id': 'status_get_interlock_fault_byte_one',
            'response': None}

    @staticmethod
    def test_status_word_validator_invalid(laser_instance):
        response_argument = {
            'type': 'laser', 'id': 'status_word', 'response': {'interlocks': '2', 'simmer': '0',
                                                               'flashlamp_sync_mode': '0', 'q_switch_sync_mode': '0'}
        }
        response = laser_instance.validator().status_word_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': "Validator error, value outside valid response limits: [{'interlocks': '2'}]",
            'id': 'status_word',
            'response': None}

    @staticmethod
    def test_status_q_switch_delay_validator_invalid(laser_instance):
        response_argument = {'type': 'laser', 'id': 'status_q_switch_delay',
                             'response': {'q_switch_delay': 1000}, 'error_message': None}

        response = laser_instance.validator().status_q_switch_delay_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message': "Validator error, value outside valid response limits 0-999: [{'q_switch_delay': 1000}]",
            'id': 'status_q_switch_delay',
            'response': None}

    @staticmethod
    def test_validate_status_get_flashlamp_frequency_invalid(laser_instance):
        response_argument = {'type': 'laser', 'id': 'status_get_flashlamp_frequency', 'response': {'frequency': 100.05}}
        response = laser_instance.validator().flashlamp_frequency_validator(response_argument)
        assert response == {
            'type': 'laser',
            'error_message':
                "Validator error, value outside valid response limits 00.000-100.00: [{'frequency': 100.05}]",
            'id': 'status_get_flashlamp_frequency',
            'response': None}

    # ----------------------------------TEST RAW RESPONSE INITIAL CHECK --------------------------------

    # ---------------------------------------- test valid cases ----------------------------------------

    @staticmethod
    def test_raw_response_initial_check(laser_instance):
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response_instance = Response(parser_mappings, validator, formatter)
        response_instance._raw_response = '\r\ndelay 100 uS   '
        response_instance._request_dictionary = {
            'type': 'laser', 'tag': 'status_q_switch_delay', 'data': None}
        checked_response = response_instance.raw_response_initial_check()
        assert checked_response is None

    # ---------------------------------------- test invalid cases ----------------------------------------

    @staticmethod
    def test_raw_response_initial_check_invalid_length(laser_instance):
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response_instance = Response(parser_mappings, validator, formatter)
        response_instance._raw_response = '\r\ndelay 777 uS'
        response_instance._request_dictionary = {
            'type': 'laser', 'tag': 'status_q_switch_delay', 'data': None}
        checked_response = response_instance.raw_response_initial_check()
        assert checked_response == {
            'type': 'laser',
            'error_message': 'Invalid response length (14), raw response is: \r\ndelay 777 uS',
            'id': 'status_q_switch_delay',
            'response': None}

    @staticmethod
    def test_raw_response_initial_check_invalid_command(laser_instance):
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response_instance = Response(parser_mappings, validator, formatter)
        response_instance._raw_response = '\r\ncmd not found  '
        response_instance._request_dictionary = {
            'type': 'laser', 'tag': 'statusss', 'data': None}
        checked_response = response_instance.raw_response_initial_check()
        assert checked_response == {
            'type': 'laser',
            'error_message': 'Invalid command (statusss)',
            'id': 'statusss',
            'response': None}

    # ---------------------------------------- TEST GET RESPONSE ---------------------------------------

    # ----------------------------------------- test valid case ----------------------------------------

    @staticmethod
    def test_get_response(laser_instance):
        request = laser_instance.response_instance._request_dictionary = {'type': 'laser', 'tag': 'status_word',
                                                                                 'data': None}
        raw_response = 'I 1 F 0 S 0 Q 0'
        parser_mappings = laser_instance.parser_mappings
        regex = laser_instance.response_instance._get_regex(request, parser_mappings)
        response = laser_instance.response_instance._get_response(request, raw_response, regex)
        for item in ['interlocks', 'flashlamp_sync_mode', 'simmer', 'q_switch_sync_mode']:
            assert item in response['response'].keys()

    # ----------------------------------------- test invalid case ----------------------------------------

    @staticmethod
    def test_get_response_invalid(laser_instance):
        request = {'type': 'laser', 'tag': 'status_word', 'data': None}
        raw_response = 'K 1 F 0 S 0 Q 0'
        parser_mappings = laser_instance.parser_mappings
        regex = laser_instance.response_instance._get_regex(request, parser_mappings)
        response = laser_instance.response_instance._get_response(request, raw_response, regex)
        assert response == {
            'type': 'laser', 'error_message': 'Parser error, raw response is: K 1 F 0 S 0 Q 0',
            'id': 'status_word',
            'response': None
        }

    # ---------------------------------------- TEST INTERNAL PROCEDURE ---------------------------------------

    @staticmethod
    def test_get_with_none_raw_response(laser_instance):
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response = Response(parser_mappings, validator, formatter)

        laser_response = response.get(request_dictionary={'type': 'laser', 'tag': 'status_get_interlock_fault_byte_one',
                                                          'data': None})
        assert laser_response is None

    @staticmethod
    def test_get_parser_invalid(laser_instance):
        """
        Test procedure from get method until parser

        Returns
        -------

        """
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response = Response(parser_mappings, validator, formatter)

        laser_response = response.get(request_dictionary={'type': 'laser', 'tag': 'status_get_interlock_fault_byte_one',
                                                          'data': None}, raw_response='\r\nIK1 01 00 00 00')
        assert laser_response == {
            'type': 'laser', 'error_message': 'Parser error, raw response is: IK1 01 00 00 00',
            'id': 'status_get_interlock_fault_byte_one',
            'response': None
        }

    @staticmethod
    def test_get_validator_invalid(laser_instance):
        """
        Test procedure from get method until validator

        Returns
        -------

        """
        parser_mappings = laser_instance.parser_mappings
        validator = laser_instance.validator
        formatter = laser_instance.formatter
        response = Response(parser_mappings, validator, formatter)

        laser_response = response.get(request_dictionary={'type': 'laser', 'tag': 'status_get_interlock_fault_byte_one',
                                                          'data': None}, raw_response='\r\nIF1 03 00 00 00')
        assert laser_response == {
            'type': 'laser',
            'error_message': "Validator error, value outside valid response limits 0-1: [{'bnc_interlock': '3'}]",
            'id': 'status_get_interlock_fault_byte_one',
            'response': None}

    # ---------------------------------------- TEST GET COMMANDS ---------------------------------------
    @staticmethod
    def test_get_commands(laser_instance):
        response = laser_instance.commands().get_commands()
        assert response == [
            'set_flashlamp_frequency', 'set_q_switch_delay', 'start_open_shutter', 'start_q_switch',
            'start_set_flashlamp_fire', 'start_set_flashlamp_simmer', 'status_get_all_three_temperatures',
            'status_get_coolant_flow_level_lpm', 'status_get_coolant_level', 'status_get_flashlamp_count',
            'status_get_flashlamp_count_since_reset', 'status_get_flashlamp_frequency',
            'status_get_interlock_fault_byte_one', 'status_get_interlock_fault_byte_three',
            'status_get_interlock_fault_byte_two', 'status_q_switch_delay', 'status_shutter', 'status_system_state',
            'status_word', 'stop_close_shutter',
            'stop_flash', 'stop_q_switch'
        ]

        logger.debug(' Method ""get_commands"" returns: {}'.format(response))

    # ---------------------------------------- TEST LASER STATUS VALIDATION ---------------------------------------

    @staticmethod
    def test_validation_status_interlock_fault_byte_one(laser_instance):

        data_1 = {'id': 'status_get_interlock_fault_byte_one',
                  'response': {'e_stop_button': True, 'bnc_interlock': False, 'laser_head_thermostat': False,
                               'laser_head_housing_switch': False, 'ice450_housing_switch': False,
                               'internal_bus_error': False, 'external_bus_error': False, 'flashlamp_timeout': False}}
        data_2 = {'id': 'status_get_interlock_fault_byte_one',
                  'response': {'e_stop_button': True, 'bnc_interlock': True, 'laser_head_thermostat': False,
                               'laser_head_housing_switch': False, 'ice450_housing_switch': False,
                               'internal_bus_error': False, 'external_bus_error': False, 'flashlamp_timeout': False}}
        data_3 = {'id': 'status_get_interlock_fault_byte_one',
                  'response': {'e_stop_button': False, 'bnc_interlock': False, 'laser_head_thermostat': False,
                               'laser_head_housing_switch': False, 'ice450_housing_switch': False,
                               'internal_bus_error': False, 'external_bus_error': False, 'flashlamp_timeout': False}}

        validation_response = laser_instance.validate_status(data_1)
        test_critical_variables1 = False, [{'e_stop_button': True}]
        assert validation_response == test_critical_variables1
        logger.debug(' Validating status_interlock_fault_byte_one.....')

        validation_response = laser_instance.validate_status(data_2)
        test_critical_variables2 = False, [{'e_stop_button': True}, {'bnc_interlock': True}]
        assert validation_response == test_critical_variables2
        logger.debug(' Validating status_interlock_fault_byte_one.....')

        validation_response = laser_instance.validate_status(data_3)
        test_critical_variables3 = True, None
        assert validation_response == test_critical_variables3
        logger.debug(' Validating status_interlock_fault_byte_one.....')

    @staticmethod
    def test_validation_status_interlock_fault_byte_two(laser_instance):

        data_1 = {'id': 'status_get_interlock_fault_byte_two',
                  'response': {'heater_thermostat': False, 'charger_temperature': True,
                               'coolant_temperature_under_max': False, 'coolant_temperature_over_max': False,
                               'coolant_level': False, 'coolant_flow': False,
                               'char_cool_or_har_generator_temp': False, 'flashlamp_power_setting': False}}
        data_2 = {'id': 'status_get_interlock_fault_byte_two',
                  'response': {'heater_thermostat': False, 'charger_temperature': True,
                               'coolant_temperature_under_max': False, 'coolant_temperature_over_max': False,
                               'coolant_level': False, 'coolant_flow': True,
                               'char_cool_or_har_generator_temp': False, 'flashlamp_power_setting': False}}
        data_3 = {
            'id': 'status_get_interlock_fault_byte_two',
            'response': {
                'heater_thermostat': False,
                'charger_temperature': False,
                'coolant_temperature_under_max': False,
                'coolant_temperature_over_max': False,
                'coolant_level': False,
                'coolant_flow': False,
                'char_cool_or_har_generator_temp': False,
                'flashlamp_power_setting': False
            }
        }

        validation_response = laser_instance.validate_status(data_1)
        test_critical_variables = False, [{'charger_temperature': True}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_two.....')

        validation_response = laser_instance.validate_status(data_2)
        test_critical_variables = False, [{'charger_temperature': True}, {'coolant_flow': True}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_two.....')

        validation_response = laser_instance.validate_status(data_3)
        test_critical_variables = True, None
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_two.....')

    @staticmethod
    def test_validation_status_interlock_fault_byte_three(laser_instance):

        data_1 = {'id': 'status_get_interlock_fault_byte_three',
                  'response': {'psu_charge_error': False, 'voltage': False, 'simmer_sensed': False,
                               'external_flash_signal_frequency_low': True,
                               'external_flash_signal_frequency_high': False, 'capacitor_discharge_problem': False,
                               'simmer_timeout': False, 'piv_slave_interlock': False}}
        data_2 = {'id': 'status_get_interlock_fault_byte_three',
                  'response': {'psu_charge_error': False, 'voltage': False, 'simmer_sensed': False,
                               'external_flash_signal_frequency_low': True,
                               'external_flash_signal_frequency_high': False, 'capacitor_discharge_problem': True,
                               'simmer_timeout': False, 'piv_slave_interlock': False}}
        data_3 = {'id': 'status_get_interlock_fault_byte_three',
                  'response': {'psu_charge_error': False, 'voltage': False, 'simmer_sensed': False,
                               'external_flash_signal_frequency_low': False,
                               'external_flash_signal_frequency_high': False, 'capacitor_discharge_problem': False,
                               'simmer_timeout': False, 'piv_slave_interlock': False}}

        validation_response = laser_instance.validate_status(data_1)
        test_critical_variables = False, [{'external_flash_signal_frequency_low': True}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_three.....')

        validation_response = laser_instance.validate_status(data_2)
        test_critical_variables = False, [{'external_flash_signal_frequency_low': True},
                                          {'capacitor_discharge_problem': True}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_three.....')

        validation_response = laser_instance.validate_status(data_3)
        test_critical_variables = True, None
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_interlock_fault_byte_three.....')

    @staticmethod
    def test_validation_status_word(laser_instance):

        data = {'id': 'status_word',
                'response': {'interlocks': True, 'simmer': False, 'flashlamp_sync_mode': 0, 'q_switch_sync_mode': 0}}

        validation_response = laser_instance.validate_status(data)
        test_critical_variables = False, [{'interlocks': True}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating status_word.....')

    @staticmethod
    def test_validation_flashlamp_count_since_reset(laser_instance):

        data_1 = {'id': 'status_get_flashlamp_count_since_reset', 'response': {'flashlamp_count_since_reset': 20000000}}
        data_2 = {'id': 'status_get_flashlamp_count_since_reset', 'response': {'flashlamp_count_since_reset': 50000000}}
        data_3 = {'id': 'status_get_flashlamp_count_since_reset', 'response': {'flashlamp_count_since_reset': 70000000}}

        validation_response = laser_instance.validate_status(data_1)
        test_critical_variables = True, None
        assert validation_response == test_critical_variables
        logger.debug(' Validating flashlamp_count_since_reset.....')

        validation_response = laser_instance.validate_status(data_2)
        test_critical_variables = True, None
        assert validation_response == test_critical_variables
        logger.debug(' Validating flashlamp_count_since_reset.....')

        validation_response = laser_instance.validate_status(data_3)
        test_critical_variables = False, [{'flashlamp_count_since_reset': 70000000}]
        assert validation_response == test_critical_variables
        logger.debug(' Validating flashlamp_count_since_reset.....')
