import pytest

from laser.tools.validation import is_boolean, is_int, is_float, _format_numbers, \
    is_within_limits


class TestValidation(object):
    def test_is_int(self):
        assert is_int(2)
        assert is_int(2222)
        assert is_int(True)
        assert not is_int('a')
        assert not is_int(1.32)

    def test_is_boolean(self):
        assert not is_boolean(2)
        assert not is_boolean(2222)
        assert is_boolean(True)
        assert not is_boolean('a')
        assert not is_boolean(1.32)

    def test_is_float(self):
        assert not is_float(2)
        assert not is_float(2222)
        assert not is_float(True)
        assert not is_float('a')
        assert is_float(1.32)
        assert is_float(1.3212121212121212121)

    def test_format_numbers(self):
        assert _format_numbers([-1, 7], fix=2, flag=None) == [-1.00, 7.00]
        assert _format_numbers([4, 6, 8], fix=4, flag=None) == [4.0000, 6.0000, 8.0000]
        assert _format_numbers([-1.111111111111, 7.8573], fix=1, flag="float or int") == [-1.1, 7.9]
        assert _format_numbers([-1.111111111111, 2], fix=2, flag="float or int") == [-1.11, 2.00]
        assert _format_numbers([7, 2], fix=2, flag="float or int") == [7.00, 2.00]
        assert _format_numbers([-1, 7.23], fix=2, flag=None) is None
        assert _format_numbers([-1, 7.23], fix=2, flag=None) is None
        assert _format_numbers(['a', 5.52, 8], fix=4, flag=None) is None
        assert _format_numbers(['a', 7.23], fix=3, flag="float or int") is None
        assert _format_numbers(['a', 'b'], fix=3, flag="float or int") is None

    @pytest.mark.parametrize("argument, down_limit, up_limit", [
        (0, 0, 0),
        (1, 0, None),
        (7, 0, None),
        (9, 5, None),
        (1, None, 1),
        (1, None, 10),
        (-1, -10, 10),
        (1, None, None),
        (-1, None, None),
        (0, None, None),
        (True, None, None)
    ])
    def test_true_is_within_limits(self, argument, down_limit, up_limit):
        assert is_within_limits(argument,  down_limit, up_limit) is True
        assert is_within_limits(None, None, None) is None

    @pytest.mark.parametrize("argument, down_limit, up_limit", [
        (-8, 0, 100),
        (3, 11, None),
        (111, None, 10),
        (-300, -10, 10)
    ])
    def test_false_is_within_limits(self, argument, down_limit, up_limit):
        assert is_within_limits(argument, down_limit, up_limit) is False
