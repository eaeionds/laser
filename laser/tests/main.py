import logging

from laser.tests.interface.argparse_controller import ArgparseController
from laser.tests.interface.ui_controller import UIController

logging.basicConfig(
    format='[%(name)-12s]: %(message)s',
    datefmt='%F %T',
    level=logging.DEBUG
)

logger = logging.getLogger(__name__)


def main():
    args = UIController().parser.parse_args()
    argparse_controller = ArgparseController(args)
    argparse_controller.run()


if __name__ == "__main__":
    main()
