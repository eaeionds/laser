from laser.tests.test_cases import base

cases = base.cases + [
    {
        'case_id': 1,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_word',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_word',
            'response': {
                'interlocks': 'No interlocks present',
                'simmer': 'Simmer is not established',
                'flashlamp_sync_mode': 'Flashlamp in Internal sync mode and not running',
                'q_switch_sync_mode': 'Q-switch in Internal sync mode and off'
            }
        }
    },
    {
        'case_id': 2,
        'simulation_functionality': 'normal',
        'test_type': 'value',
        'command': {
            'type': 'laser',
            'tag': 'status_system_state',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': 'Invalid command (status_system_state)',
            'id': 'status_system_state',
            'response': None
        }
    },
    {
        'case_id': 3,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_interlock_fault_byte_one',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_interlock_fault_byte_one',
            'response': {
                'e_stop_button': False,
                'bnc_interlock': False,
                'laser_head_thermostat': False,
                'laser_head_housing_switch': False,
                'ice450_housing_switch': False,
                'internal_bus_error': False,
                'external_bus_error': False,
                'flashlamp_timeout': False
            }
        }
    },
    {
        'case_id': 4,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_interlock_fault_byte_two',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_interlock_fault_byte_two',
            'response': {
                'heater_thermostat': False,
                'charger_temperature': False,
                'coolant_temperature_under_max': False,
                'coolant_temperature_over_max': False,
                'coolant_level': False,
                'coolant_flow': False,
                'char_cool_or_har_generator_temp': False,
                'flashlamp_power_setting': False
            }
        }
    },
    {
        'case_id': 5,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_interlock_fault_byte_three',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_interlock_fault_byte_three',
            'response': {
                'psu_charge_error': False,
                'voltage': False,
                'simmer_sensed': False,
                'ext_flash_signal_frequency_low': False,
                'ext_flash_signal_frequency_high': False,
                'capacitor_discharge_problem': False,
                'simmer_timeout': False,
                'piv_slave_interlock': False
            }
        }
    },
    {
        'case_id': 6,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_coolant_flow_level_lpm',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_coolant_flow_level_lpm',
            'response': {
                'coolant_flow': 2.45
            }
        }
    },
    {
        'case_id': 7,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_all_three_temperatures',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_all_three_temperatures',
            'response': {
                'cg_temperature': 372,
                'shg_temperature': 553,
                'cs_temperature': 561
            }
        }
    },
    {
        'case_id': 8,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_coolant_level',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_coolant_level',
            'response': {
                'coolant_level': 'OK'
            }
        }
    },
    {
        'case_id': 9,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_flashlamp_count',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_flashlamp_count',
            'response': {
                'flashlamp_count': 20000000
            }
        }
    },
    {
        'case_id': 10,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_flashlamp_count_since_reset',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_flashlamp_count_since_reset',
            'response': {
                'flashlamp_count_since_reset': 20000000
            }
        }
    },
    {
        'case_id': 11,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_q_switch_delay',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_q_switch_delay',
            'response': {
                'q_switch_delay': 2
            }
        }
    },
    {
        'case_id': 12,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_get_flashlamp_frequency',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_get_flashlamp_frequency',
            'response': {
                'frequency': 0.02
            }
        }
    },
    {
        'case_id': 13,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'status_shutter',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'status_shutter',
            'response': {
                'shutter_status': 'CLOSE'
            }
        }
    },
    {
        'case_id': 14,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'start_set_flashlamp_simmer',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'start_set_flashlamp_simmer',
            'response': {
                'must_be_simmer': 'simmer         '
            }
        }
    },
    {
        'case_id': 15,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'set_q_switch_delay',
            'data': 1
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'set_q_switch_delay',
            'response': {
                'q_switch_delay': 1  # 1 is the supported command arg in drivers
            }
        }
    },
    {
        'case_id': 16,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'start_set_flashlamp_fire',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'start_set_flashlamp_fire',
            'response': {
                'state': 'fire auto      '
            }
        }
    },
    {
        'case_id': 17,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'start_q_switch',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'start_q_switch',
            'response': {
                'state': 'fire auto qs   '
            }
        }
    },
    {
        'case_id': 18,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'start_open_shutter',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'start_open_shutter',
            'response': {
                'is_open': 'OPEN'
            }
        }
    },
    {
        'case_id': 19,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'stop_q_switch',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'stop_q_switch',
            'response': {
                'state': 'fire auto      '
            }
        }
    },
    {
        'case_id': 20,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'stop_flash',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'stop_flash',
            'response': {
                'is_standby': 'standby        '
            }
        }
    },
    {
        'case_id': 21,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'stop_close_shutter',
            'data': None
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'stop_close_shutter',
            'response': {
                'is_close': 'CLOSE'
            }
        }
    },
    {
        'case_id': 22,
        'simulation_functionality': 'normal',
        'test_type': 'keys',
        'command': {
            'type': 'laser',
            'tag': 'set_flashlamp_frequency',
            'data': 2  # 2 is the supported command arg in drivers
        },
        'response': {
            'type': 'laser',
            'error_message': None,
            'id': 'set_flashlamp_frequency',
            'response': {
                'frequency': 0.02
            }
        }
    },
    # {
    #     'case_id': 23,
    #     'simulation_functionality': 'error',
    #     'test_type': 'keys',
    #     'command': {
    #         'type': 'laser',
    #         'tag': 'set_flashlamp_frequency',
    #         'data': 3.1
    #     },
    #     'response': {
    #         "type": "laser",
    #         "error_message": 'flashlamp frequency input must be integer',
    #         "id": "set_flashlamp_frequency",
    #         "response": None
    #     }
    # },
    # {
    #     'case_id': 24,
    #     'simulation_functionality': 'error',
    #     'test_type': 'keys',
    #     'command': {
    #         'type': 'laser',
    #         'tag': 'set_flashlamp_frequency',
    #         'data': None
    #     },
    #     'response': {
    #         "type": "laser",
    #         "error_message": 'You must enter a value for flashlamp_frequency',
    #         "id": "set_flashlamp_frequency",
    #         "response": None
    #     }
    # },
    # {
    #     'case_id': 25,
    #     'simulation_functionality': 'error',
    #     'test_type': 'keys',
    #     'command': {
    #         'type': 'laser',
    #         'tag': 'set_flashlamp_frequency',
    #         'data': 300000
    #     },
    #     'response': {
    #         "type": "laser",
    #         "error_message": 'frequency out of range: 0 - 100',
    #         "id": "set_flashlamp_frequency",
    #         "response": None
    #     }
    # }
]
