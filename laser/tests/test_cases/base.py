"""
Basic structure

case_id: int -- Test case id
simulation_functionality: normal|errors -- Describe the Simulator operation
test_type: keys|value|partial -- Describe the response's comparison type
command: {'type': 'laser', 'tag': 'version', 'data': None},
response: {
    'type': 'laser',
    'error_message': None,
    'id': 'version',
    'response': {}
}
"""
cases = []
