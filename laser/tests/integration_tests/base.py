import logging
import time

logger = logging.getLogger(__name__)


class Base(object):
    """
        This is the base of integration tests.

        Based on selected Laser version, it loads the proper cases.
        Starts the process of sending and retrieving commands/responses.
        Validates the responses based on command and the test_type.
    """

    def __init__(self, laser_instance):
        self.laser_instance = laser_instance

    def run(self, simulation_functionality, case_id, test_type, command, expected_response):

        def print_logs():
            logger.info(
                '############################# Test case: {} #############################'.format(
                    case_id
                ))
            logger.info('Command: {}'.format(command))
            logger.info('Expected response: {}'.format(expected_response))

        def print_ignore_logs():
            logger.info('################################# PASSING ERROR CASE #################################')

        print_logs()
        if simulation_functionality == 'errors':
            print_ignore_logs()
            return

        self.laser_instance.connect()
        self.laser_instance.write(command)

        is_matched = False
        counter = 20
        response = None
        while counter > 0:
            response = self.laser_instance.read_line(command)
            if response is not None and len(response) > 0:
                logger.info('Response: {}'.format(response))
                if test_type == 'keys':
                    if response['response'].keys() == expected_response['response'].keys():
                        logger.info("Matched by keys!!!")
                        is_matched = True
                        break
                elif test_type == 'partial':
                    pass
                elif test_type == 'value':
                    if response == expected_response:
                        break
                else:
                    logger.error('test_type is not defined...')
                    break
            counter -= 0.001
            time.sleep(0.001)
        self.laser_instance.disconnect()
        if not is_matched:
            assert expected_response == response
