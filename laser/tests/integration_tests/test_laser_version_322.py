import logging
import pytest

from laser.tests.integration_tests.base import Base
from laser.tests.test_cases import version_322

logger = logging.getLogger(__name__)


class TestLaserVersion322(object):

    @staticmethod
    @pytest.mark.parametrize("case_id, simulation_functionality, test_type, command, expected_response", [
        (item['case_id'], item['simulation_functionality'], item['test_type'], item['command'], item['response'])
        for item in version_322.cases
    ])
    def test_command(laser_instance, simulation_functionality, case_id, test_type, command, expected_response):
        Base(laser_instance).run(
            simulation_functionality, case_id, test_type, command, expected_response
        )
