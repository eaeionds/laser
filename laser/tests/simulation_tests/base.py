import logging
import time

from laser.simulators.serial_simulator.serial_simulator import SerialSimulator

logger = logging.getLogger(__name__)


class Base(object):
    """
    This is the base of unit tests.

    Based on selected Laser version, it loads the proper cases.
    Initialize Simulator operation mode accordingly.
    It mocks the Serial with the Simulator.
    Starts the process of sending and retrieving commands/responses.
    Validates the responses based on command and the test_type.
    """

    def __init__(self, laser_instance, normal, errors, supported_commands):
        self.laser_instance = laser_instance
        self.normal = normal
        self.errors = errors
        self.break_flag = False
        self.supported_commands = supported_commands

    def run(self, MockSerial, simulation_functionality, case_id, test_type, command, expected_response):

        def print_logs():
            logger.info(
                '############################# Test case: {} -- functionality: {} #############################'.format(
                    case_id, simulation_functionality
                ))
            logger.info('Command: {}'.format(command))
            logger.info('Expected response: {}'.format(expected_response))
            logger.info('')

        def run_command(*args, **kwargs):
            if args[0] not in self.supported_commands:
                logger.error('Command: {} not included in simulation driver.'.format(args[0]))
                logger.error('Supported commands: {}'.format(self.supported_commands))
                self.break_flag = True
            return serial_simulator_instance.write(args[0])

        # Initialize simulation
        if simulation_functionality == 'normal':
            SerialSimulator.test_cases = self.normal
        elif simulation_functionality == 'errors':
            SerialSimulator.test_cases = self.errors
        serial_simulator_instance = SerialSimulator()

        print_logs()

        # Initialize mocking
        serial = MockSerial()
        serial.connect.return_value = serial_simulator_instance.connect()
        serial.read.return_value = serial_simulator_instance.read()
        serial.write.side_effect = run_command

        # Test
        self.laser_instance.serial = serial
        self.laser_instance.connect()
        self.laser_instance.write(command)

        if self.break_flag:
            assert False

        is_matched = False
        counter = 10
        response = None
        while counter > 0:
            serial.read.return_value = serial_simulator_instance.read()
            response = self.laser_instance.read_line(command)
            if response is not None and len(response) > 0:
                if test_type == 'keys':
                    if response['response'].keys() == expected_response['response'].keys():
                        logger.info("Matched by keys!!!")
                        is_matched = True
                        break
                elif test_type == 'partial':
                    pass
                elif test_type == 'value':
                    if response == expected_response:
                        break
                else:
                    logger.error('test_type is not defined...')
                    break
            counter -= 0.001
            time.sleep(0.001)
        self.laser_instance.disconnect()
        if not is_matched:
            assert expected_response == response
