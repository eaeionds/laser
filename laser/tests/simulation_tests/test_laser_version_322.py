import logging

import pytest
from mock import patch

from laser.laser_main.laser_instance import LaserVersion322
from laser.simulators.serial_simulator.drivers.version_322 import all_cases, normal, errors
from laser.tests.simulation_tests.base import Base
from laser.tests.test_cases import version_322

logger = logging.getLogger(__name__)


class TestLaserVersion322(object):

    @staticmethod
    @pytest.mark.parametrize("case_id, simulation_functionality, test_type, command, expected_response", [
        (item['case_id'], item['simulation_functionality'], item['test_type'], item['command'], item['response'])
        for item in version_322.cases
    ])
    @patch('laser.laser_base.serial.Serial')
    def test_commands(MockSerial, simulation_functionality, case_id, test_type, command, expected_response):
        supported_commands = list()
        for case in all_cases:
            supported_commands.append(case['command'])
        Base(LaserVersion322(), normal, errors, supported_commands).run(
            MockSerial, simulation_functionality, case_id, test_type, command, expected_response
        )
