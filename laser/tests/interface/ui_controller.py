import argparse

from laser.laser_base.interface.ui_controller_base import UIControllerBase


class UIController(UIControllerBase):

    def __init__(self):
        super().__init__()
        self._parser = argparse.ArgumentParser(
            description='Laser testing framework.\t\tGet test cases'
            '\n\t\t\t\t\trun unit and integration tests',
            formatter_class=argparse.RawTextHelpFormatter,
            epilog='Company: Raymetrics'
        )

    def _set_commands(self):
        """
        Set of commands regarding component firmware.

        Returns
        -------

        """
        group_laser_commands = self._parser.add_argument_group('Testing options')
        commands_group = group_laser_commands.add_mutually_exclusive_group()

        commands_group.add_argument(
            '--cases',
            action='store_true',
            help='\t\t\tget test cases for selected version',
            default=False
        )

        commands_group.add_argument(
            '--unit',
            action='store_true',
            help='\t\t\trun unit tests for selected version',
            default=False
        )

        commands_group.add_argument(
            '--simulation',
            action='store_true',
            help='\t\t\trun simulation tests for selected version',
            default=False
        )

        commands_group.add_argument(
            '--integration',
            action='store_true',
            help='\t\t\trun integration tests for selected version',
            default=False
        )
