import logging
import os
import pytest

from laser.laser_base.interface.argparse_controller_base import ArgparseControllerBase
from laser.tests import unit_tests, integration_tests, simulation_tests

logger = logging.getLogger(__name__)


class ArgparseController(ArgparseControllerBase):

    def __init__(self, args):
        super().__init__(args)
        self.path_unit = os.path.dirname(unit_tests.__file__)
        self.path_simulation = os.path.dirname(simulation_tests.__file__)
        self.path_integration = os.path.dirname(integration_tests.__file__)

    def _set_up(self):
        version_id = self.selected_version['version_id']

        if self.args.cases:
            self.lock = True
            if version_id == '322':
                from laser.tests.test_cases.version_322 import cases
                for case in cases:
                    print(case['command'])
            else:
                print('Version: {} is not supported...'.format(self.args.version))

        if self.args.unit:
            pytest.main(
                [
                    '--additional_arguments', '{}_{}'.format(self.args.version, self.args.com),
                    '{path_unit}'.format(path_unit=self.path_unit)
                ]
            )
            self.lock = True

        if self.args.simulation:
            self.lock = True
            if version_id == '322':
                pytest.main(['{path_simulation}/test_laser_version_322.py'.format(
                    path_simulation=self.path_simulation
                )])
            else:
                print('Version: {} is not supported...'.format(self.args.version))

        if self.args.integration:
            self.lock = True
            if version_id == '322':
                pytest.main(
                    [
                        '--additional_arguments', '{}_{}'.format(self.args.version, self.args.com),
                        '{path_integration}/test_laser_version_322.py'.format(
                            path_integration=self.path_integration
                        )
                    ]
                )
            else:
                print('Version: {} is not supported...'.format(self.args.version))

    def _execute_command(self):
        if self.args.com and self.selected_version is not None and 'version_id' in self.selected_version:
            self._set_up()
        elif self.selected_version is None:
            print('You must enter a valid laser version.\nFor example: laser_test --version 322')
