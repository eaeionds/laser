import logging

from laser.tools.validation import is_int, is_within_limits

logger = logging.getLogger(__name__)


class CommandsBase(object):

    @staticmethod
    def get_commands():
        """
        Get all laser's available commands

        Returns
        -------
        list
            all available generic commands
        """
        commands = [func for func in dir(CommandsBase) if callable(getattr(CommandsBase, func)) and
                    not func.startswith('__') and not func.startswith('get_commands')
                    and not func.startswith('execute_request_function')
                    and not func.startswith('invalid_input_setup')]
        return commands

    @classmethod
    def execute_request_function(cls, request_function_name, request_data):
        """
        Checks if there are additional data to the command for setup and executes request function

        Parameters
        ----------
        request_function_name: str
                command (e.g. 'WOR')
        request_data
            data for laser input, e.g. '2' in PMOD2 command

        Returns
        -------
        str
            e.g. 'IF2'

        """
        if request_function_name in cls.get_commands():
            if request_data is not None:
                if isinstance(request_data, dict):
                    return getattr(cls, request_function_name)(**request_data)
                else:
                    return getattr(cls, request_function_name)(request_data)
            else:
                return getattr(cls, request_function_name)()

    @classmethod
    def status_word(cls):
        """
        Queries the state of the laser.

        Warnings
        --------
        This command is included for compatibility with the Brilliant ICE450.

        Notes
        -----
        The response is: I a F b S c Q d, where:
        a: “0” = No interlocks present;
        “1” = Interlocks present
        b: ”0” = The flashlamp is in Internal sync mode and is not running;
        ”2” = The flashlamp is in Internal sync mode and is running;
        ”4” = The flashlamp is in External sync mode and is not running;
        ”6” = The flashlamp is in External sync mode and is running.
        c: “1” = Simmer is established;
        “0” = Simmer is not established
        d: ”0” = The Q-Switch is in Internal sync and is off;
        ”1” = The Q-Switch is in Internal sync and is in single mode;
        ”2” = The Q-Switch is in Internal sync and is running;
        ”4” = The Q-Switch is in External sync and is off;
        ”6” = The Q-Switch is in External sync and is running.

        Returns
        -------
        str
            'WOR'

        """
        return 'WOR'

    @classmethod
    def status_system_state(cls):
        """
        Queries the system state.

        Notes
        -----
        The response to this command is the “state string” that tells you the basic state of the laser, including:
        •	“standby”: This is the state string when the laser is not firing and not simmering.
        •	“simmer”: This the state string when the laser is only simmering.
        •	”fire auto”: This is the state string when the flashlamp is firing in Internal sync mode.
        •	”fire ext”: This is the state string when the flashlamp is firing in External sync mode.
        •	If the flashlamp is firing, and the Q-Switch is enabled, the word “qs” is added,
        and if the Q-Switch sync mode is External, the letter “e” will be added.
        For example, if the flashlamp is firing in External sync mode and the Q-Switch is running in External sync mode,
        the state string will be ”fire ext qs e”. •	”fire auto qs”: This means the flashlamp is firing in Internal sync
        and the Q-Switch is operating in Internal sync.
        •	”fire ext qs”: This means the flashlamp is firing in External sync
        and the Q-Switch is operating in Internal sync.

        Returns
        -------
        str
            'ST'

        """
        return 'ST'

    @classmethod
    def status_get_interlock_fault_byte_one(cls):
        """
        Queries Interlock Fault Byte 1

        Notes
        -----
        The response string contains ASCII-encoded 1’s or 0’s indicating either a fault (1) or okay (0)
        condition exists. The format of the string is as follows: IF1 ab cd ef gh, where the letters correspond
        to the following interlocks:
        a: “1” if e-stop button is pressed “0” if not
        b: “1” if bnc interlock is open “0” if not
        c: “1” if Laser Head thermostat is open “0” if not
        d: “1” if Laser Head housing switch is open “0” if not
        e: “1” if ICE450 housing switch is open “0” if not
        f: “1” if there is an Internal bus error “0” if not
        g: “1” if there is an External bus error “0” if not
        h: “1” if flashlamp timeout “0” if not

        Returns
        -------
        str
            'IF1'

        """
        return 'IF1'

    @classmethod
    def status_get_interlock_fault_byte_two(cls):
        """
        Queries Interlock Fault Byte 2

        Notes
        -----
        The response string contains ASCII-encoded 1’s or 0’s indicating that either a fault or okay condition exists.
        a: “1” if heater thermostat is open “0” if not
        b: “1” if charger temperature is over the maximum temperature setting “0” if not
        c: “1” if coolant temperature is under the minimum temperature setting “0” if not
        d: “1” if coolant temperature is over the maximum temperature setting “0” if not
        e: “1” if coolant level is low “0” if not
        f: “1” if coolant flow is low “0” if not
        g: “1” if charger, coolant, or harmonic generator temperature is below the minimum setting “0” if not
        h: “1” if flashlamp power setting is too high “0” if not

        Returns
        -------
        str
            'IF2'

        """
        return 'IF2'

    @classmethod
    def status_get_interlock_fault_byte_three(cls):
        """
        Queries Interlock Fault Byte 3

        Notes
        -----
        The response string contains ASCII-encoded 1’s or 0’s indicating that either a fault or okay condition exists.
        a: “1” if no end of charge prior to fire order (PSU charge error) “0” if not
        b: “1” if voltage is over setting “0” if not
        c: “1” if no simmer sensed “0” if not
        d: “1” if external flash signal frequency is too low “0” if not
        e: “1” if external flash signal frequency is too high “0” if not
        f: “1” if there is a capacitor discharge problem “0” if not
        g: “1” if there is simmer timeout “0” if not
        h: “1” if the PIV slave indicates master unit has interlock and vice versa “0” if not

        Returns
        -------
        str
            'IF3'

        """
        return 'IF3'

    @classmethod
    def status_get_coolant_flow_level_lpm(cls):
        """
        Queries the coolant flow rate in liters per minute (lpm)

        Returns
        -------
        str
            'FLOW'

        """
        return 'FLOW'

    @classmethod
    def status_get_all_three_temperatures(cls):
        """
        Queries all three temperatures at once.

        Notes
        -----
        Reports CG, SHG and CS temperatures in tenths of degrees Celsius in a single string.
        A space character separates the temperatures.

        Returns
        -------
        str
            'T3'

        """
        return 'T3'

    @classmethod
    def status_get_coolant_level(cls):
        """
        Queries the coolant level status

        Returns
        -------
        str
            'LEV'

        """
        return 'LEV'

    @classmethod
    def status_get_flashlamp_count(cls):
        """
        Queries the total number of flashlamp firings since shipment from the factory.

        Returns
        -------
        str
            'F'

        """
        return 'F'

    @classmethod
    def status_get_flashlamp_count_since_reset(cls):
        """
        Queries the number of flashlamp firings since the resetting of the count.

        Notes
        -----
        Reset the flashlamp count with every flashlamp replacement.

        Returns
        -------
        str
            'UF'

        """
        return 'UF'

    @classmethod
    def status_q_switch_delay(cls):
        """
        Queries the Q-switch delay setting.

        Notes
        -----
        Its an amount of time in microseconds, between the flashlamp activation and
        the Q-Switch pulse when the Q-Switch is in internal mode.

        Returns
        -------
        str
            'W'

        """
        return 'W'

    @classmethod
    def status_shutter(cls):
        """
        Command to query the status of shutter.

        Returns
        -------
        str
            'SHC'

        """
        return 'SHC'

    @classmethod
    def status_get_flashlamp_frequency(cls):
        """
        Queries the flashlamp firing frequency.

        Notes
        -----
        This is the rate at which the flashlamp fires when in Internal Synchronization mode.
        When the flashlamp is in External Sync, the external trigger frequency compares with this setting.

        Returns
        -------
        str
            'D'

        """
        return 'D'

    @classmethod
    def start_set_flashlamp_simmer(cls):
        """
        Sets the flashlamp into Simmer mode.

        Notes
        -----
        The laser's response to this command is the state string.
        If the flashlamp is able to start simmering, the response will be “simmer”.
        If not, the response will indicate the state

        Returns
        -------
        str
            'M'

        """
        return 'M'

    @classmethod
    def start_set_flashlamp_fire(cls):
        """
        Sets the flashlamp into “Internal” sync mode and to start flashing.

        Notes
        -----
        The response to this command is the state string.
        If the flashlamp is able to start flashing, the response will be “fire auto”.
        If not, the response will indicate the state.

        Returns
        -------
        str
            'A'

        """
        return 'A'

    @classmethod
    def start_q_switch(cls):
        """
        Starts the Q-Switching.

        Notes
        -----
        The response to this command is the state string

        Returns
        -------
        str
            'CC'

        """
        return 'CC'

    @classmethod
    def start_open_shutter(cls):
        """
        Command to open the shutter.

        Returns
        -------
        str
            'SHC1'

        """
        return 'SHC1'

    @classmethod
    def stop_q_switch(cls):
        """
        Disables the Q-Switch.

        Notes
        -----
        The response to this command is the state string.

        Returns
        -------
        str
            'CS'

        """
        return 'CS'

    @classmethod
    def stop_flash(cls):
        """
        Stops the flashlamp from flashing and turns off the simmer.

        Notes
        -----
        The response to this command is the state string, and it should be “standby”

        Returns
        -------
        str
            'S'

        """
        return 'S'

    @classmethod
    def stop_close_shutter(cls):
        """
        Command to close the shutter.

        Returns
        -------
        str
            'SHC0'

        """
        return 'SHC0'

    @classmethod
    def set_q_switch_delay(cls, delay=None):
        """
        Sets a delay for q-switch

        Parameters
        ----------
        delay: int
            new Q-switch delay

        Returns
        -------
        str
            'W[int]

        """
        command = 'W{delay}'.format(delay=int(delay))
        if delay is not None:
            if is_int(delay):
                if is_within_limits(delay, 180, 400):
                    logger.debug('Valid command')
                else:
                    logger.debug('delay out of range: 180 - 400')
            else:
                logger.debug("Q-Switch delay must be integer")
        else:
            logger.debug("You must enter a value for Q-Switch delay")
        return command

    @classmethod
    def set_flashlamp_frequency(cls, frequency=None):
        """
        Sets the flashlamp firing frequency.

        Parameters
        ----------
        frequency: int
            new flashlamp firing frequency

        Notes
        -----
        Numbers fill starting from less significant position

        Returns
        -------
        str
            'D[int]

        """
        if frequency is not None:
            if is_int(frequency):
                if is_within_limits(frequency, 0, 10000):
                    logger.debug('Valid command')
                    return 'D{frequency}'.format(frequency=int(frequency))
                else:
                    return cls.invalid_input_setup(
                        method_name='set_flashlamp_frequency',
                        error_message='frequency out of range: 0 - 100'
                    )
            else:
                return cls.invalid_input_setup(
                    method_name='set_flashlamp_frequency',
                    error_message='flashlamp frequency input must be integer'
                )
        else:
            return cls.invalid_input_setup(
                method_name='set_flashlamp_frequency',
                error_message='You must enter a value for flashlamp_frequency'
            )

    @staticmethod
    def invalid_input_setup(method_name, error_message):
        response = dict()
        response['type'] = 'laser'
        response['error_message'] = error_message
        response['id'] = method_name
        response['response'] = None
        return response
