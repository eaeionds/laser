from laser.tools import data_types


response_mappings_base = {

    'status_word': [
        {'code': 'interlocks',          'description': 'Indicates presence of Interlocks',   'type': data_types.BOOL},
        {'code': 'flashlamp_sync_mode', 'description': 'Flashlamp Sync Mode',                'type': data_types.INT},
        {'code': 'simmer',              'description': 'Indicates Simmer Establishment',     'type': data_types.BOOL},
        {'code': 'q_switch_sync_mode',  'description': 'Q-Switch Sync Mode',                 'type': data_types.INT}
    ],
    'status_get_interlock_fault_byte_one': [
        {
            'code': 'e_stop_button',
            'description': 'Indicates if e-stop button is pressed',
            'type': data_types.BOOL
        },
        {
            'code': 'bnc_interlock',
            'description': 'Indicates if bnc interlock is open',
            'type': data_types.BOOL
        },
        {
            'code': 'laser_head_thermostat',
            'description': 'Indicates if Laser Head thermostat is open',
            'type': data_types.BOOL
        },
        {
            'code': 'laser_head_housing_switch',
            'description': 'Indicates if Laser Head switch is open',
            'type': data_types.BOOL
        },
        {
            'code': 'ice450_housing_switch',
            'description': 'Indicates if ICE450 housing switch switch is open',
            'type': data_types.BOOL
        },
        {
            'code': 'internal_bus_error',
            'description': 'Indicates if there is an internal bus error',
            'type': data_types.BOOL
        },
        {
            'code': 'external_bus_error',
            'description': 'Indicates if there is an external bus error',
            'type': data_types.BOOL
        },
        {
            'code': 'flashlamp_timeout',
            'description': 'Indicates flashlamp timeout',
            'type': data_types.BOOL
        }
    ],
    'status_get_interlock_fault_byte_two': [
        {
            'code': 'heater_thermostat',
            'description': 'Indicates if heater thermostat is open',
            'type': data_types.BOOL
        },
        {
            'code': 'charger_temperature',
            'description': 'Indicates if charger temperature is over the maximum temperature setting',
            'type': data_types.BOOL
        },
        {
            'code': 'coolant_temperature_under_max',
            'description': 'Indicates if coolant temperature is under the minimum temperature setting',
            'type': data_types.BOOL
        },
        {
            'code': 'coolant_temperature_over_max',
            'description': 'Indicates if coolant temperature is over the maximum temperature setting',
            'type': data_types.BOOL
        },
        {
            'code': 'coolant_level',
            'description': 'Indicates if coolant level is low',
            'type': data_types.BOOL
        },
        {
            'code': 'coolant_flow',
            'description': 'Indicates if coolant flow is low',
            'type': data_types.BOOL
        },
        {
            'code': 'char_cool_or_har_generator_temp',
            'description': 'Indicates if charger, coolant, or harmonic generator temperature is below the min setting',
            'type': data_types.BOOL
        },
        {
            'code': 'flashlamp_power_setting',
            'description': 'Indicates if flashlamp power setting is too high',
            'type': data_types.BOOL
        }
    ],
    'status_get_interlock_fault_byte_three': [
        {
            'code': 'psu_charge_error',
            'description': 'Indicates if no end of charge prior to fire order (PSU charge error)',
            'type': data_types.BOOL
        },
        {
            'code': 'voltage',
            'description': 'Indicates if voltage is over setting',
            'type': data_types.BOOL
        },
        {
            'code': 'simmer_sensed',
            'description': 'Indicates if no simmer sensed',
            'type': data_types.BOOL
        },
        {
            'code': 'ext_flash_signal_frequency_low',
            'description': 'Indicates if external flash signal frequency is too low',
            'type': data_types.BOOL
        },
        {
            'code': 'ext_flash_signal_frequency_high',
            'description': 'Indicates if external flash signal frequency is too high',
            'type': data_types.BOOL
        },
        {
            'code': 'capacitor_discharge_problem',
            'description': 'Indicates if there is a capacitor discharge problem',
            'type': data_types.BOOL
        },
        {
            'code': 'simmer_timeout',
            'description': 'Indicates if there is simmer timeout',
            'type': data_types.BOOL
        },
        {
            'code': 'piv_slave_interlock',
            'description': 'Indicates if the PIV slave indicates master unit has interlock and vice versa',
            'type': data_types.BOOL
        }
    ],
    'status_get_coolant_flow_level_lpm': [
        {
            'code': 'coolant_flow',
            'description': 'Coolant flow rate in liters per minute',
            'type': data_types.FLOAT
        }
    ],
    'status_get_all_three_temperatures': [
        {
            'code': 'cg_temperature',
            'description': 'CG temperature in tenths of degrees Celsius',
            'type': data_types.INT
        },
        {
            'code': 'shg_temperature',
            'description': 'SHC temperature in tenths of degrees Celsius',
            'type': data_types.INT
        },
        {
            'code': 'cs_temperature',
            'description': 'CS temperature in tenths of degrees Celsius',
            'type': data_types.INT
        }
    ],
    'status_get_coolant_level': [
        {
            'code': 'coolant_level',
            'description': 'Coolant level status',
            'type': data_types.STRING
        }
    ],
    'status_get_flashlamp_count_since_reset': [
        {
            'code': 'flashlamp_count_since_reset',
            'description': 'number of flashlamp firings since the resetting of the count',
            'type': data_types.INT
        }
    ],
    'status_get_flashlamp_count': [
        {
            'code': 'flashlamp_count',
            'description': 'number of flashlamp firings',
            'type': data_types.INT
        }
    ],
    'status_q_switch_delay': [
        {
            'code': 'q_switch_delay',
            'description': 'Q-Switch delay',
            'type': data_types.INT
        }
    ],
    'flashlamp_frequency': [
        {
            'code': 'frequency',
            'description': 'Flashlamp firing frequency',
            'type': data_types.FLOAT
        }
    ]
}
