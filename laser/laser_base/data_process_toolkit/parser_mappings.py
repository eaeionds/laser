from laser.laser_base.data_process_toolkit.response_mappings import response_mappings_base


class ParserMappingsBase(object):
    mappings = response_mappings_base

    parser_mapper_dict = {
            'status_word': {
                'regex': '^I\s+(?P<interlocks>\d+)\s+F\s+(?P<flashlamp_sync_mode>\d+)\s+S\s+(?P<simmer>\d+)\s+Q\s+'
                         '(?P<q_switch_sync_mode>\d+)\s*',
                'mapping': mappings['status_word']
            },
            'status_system_state': {
                'regex': '\s*(?P<system_state>[\w ]+\\b)\s*',
                'mapping': None
            },
            'status_get_interlock_fault_byte_one': {
                'regex': '^IF1\s*(?P<e_stop_button>\d)\s*(?P<bnc_interlock>\d)\s*(?P<laser_head_thermostat>\d)\s*'
                         '(?P<laser_head_housing_switch>\d)\s*(?P<ice450_housing_switch>\d)\s*'
                         '(?P<internal_bus_error>\d)\s*(?P<external_bus_error>\d)\s*(?P<flashlamp_timeout>\d)\s*',
                'mapping': mappings['status_get_interlock_fault_byte_one']
            },
            'status_get_interlock_fault_byte_two': {
                'regex': '^IF2\s*(?P<heater_thermostat>\d)\s*(?P<charger_temperature>\d)\s*'
                         '(?P<coolant_temperature_under_max>\d)\s*(?P<coolant_temperature_over_max>\d)\s*'
                         '(?P<coolant_level>\d)\s*(?P<coolant_flow>\d)\s*(?P<char_cool_or_har_generator_temp>\d)\s*'
                         '(?P<flashlamp_power_setting>\d)\s*',
                'mapping': mappings['status_get_interlock_fault_byte_two']
            },
            'status_get_interlock_fault_byte_three': {
                'regex': '^IF3\s*(?P<psu_charge_error>\d)\s*(?P<voltage>\d)\s*(?P<simmer_sensed>\d)\s*'
                         '(?P<ext_flash_signal_frequency_low>\d)\s*(?P<ext_flash_signal_frequency_high>\d)\s*'
                         '(?P<capacitor_discharge_problem>\d)\s*(?P<simmer_timeout>\d)\s*'
                         '(?P<piv_slave_interlock>\d)\s*',
                'mapping': mappings['status_get_interlock_fault_byte_three']
            },
            'status_get_coolant_flow_level_lpm': {
                'regex': '^FLOW\s*(?P<coolant_flow>[\d.]+)\s*lpm\s*',
                'mapping': mappings['status_get_coolant_flow_level_lpm']
            },
            'status_get_all_three_temperatures': {
                'regex': '^T3\s*(?P<cg_temperature>[\d]+)\s*(?P<shg_temperature>[\d]+)\s*(?P<cs_temperature>[\d]+)\s*',
                'mapping': mappings['status_get_all_three_temperatures']
            },
            'status_get_coolant_level': {
                'regex': '^LEV\s*(?P<coolant_level>OK|LOW)\s*',
                'mapping': mappings['status_get_coolant_level']
            },
            'status_get_flashlamp_count_since_reset': {
                'regex': '^cu\s*LP\s*(?P<flashlamp_count_since_reset>\d+)\s*',
                'mapping': mappings['status_get_flashlamp_count_since_reset']
            },
            'status_get_flashlamp_count': {
                'regex': '^cu\s*LP\s*(?P<flashlamp_count>\d+)\s*',
                'mapping': mappings['status_get_flashlamp_count']
            },
            'status_q_switch_delay': {
                'regex': 'delay\s*(?P<q_switch_delay>\d+)\s*uS\s*',
                'mapping': mappings['status_q_switch_delay']
            },
            'start_set_flashlamp_simmer': {
                'regex': '(?P<must_be_simmer>[a-z\s]+)',
                'mapping': None
            },
            'start_set_flashlamp_fire': {
                'regex': '(?P<state>[a-z\s]+)',
                'mapping': None
            },
            'start_q_switch': {
                'regex': '(?P<state>[a-z\s]+)',
                'mapping': None
            },
            'start_open_shutter': {
                'regex': 'shc:\s*(?P<is_open>[OPEN|CLOSE]+)\s*',
                'mapping': None
            },
            'stop_q_switch': {
                'regex': '(?P<state>[a-z\s]+)',
                'mapping': None
            },
            'stop_flash': {
                'regex': '(?P<is_standby>[a-z\s]+)',
                'mapping': None
            },
            'stop_close_shutter': {
                'regex': 'shc:\s*(?P<is_close>[OPEN|CLOSE]+)\s*',
                'mapping': None
            },
            'flashlamp_frequency': {
                'regex': 'freq.?\s*(?P<frequency>\d+.\d+)[a-zA-Z ]*',
                'mapping': mappings['flashlamp_frequency']
            },
            'status_shutter': {
                'regex': 'shc:\s*(?P<shutter_status>OPEN|CLOSE\\b)\s*',
                'mapping': None
            }
        }
