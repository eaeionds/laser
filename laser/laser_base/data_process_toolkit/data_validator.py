import logging
from laser.tools.validation import is_within_limits

logger = logging.getLogger(__name__)


class ValidatorBase(object):

    validator_mapper = [
        {'tag': ['status_get_interlock_fault_byte_one',
                 'status_get_interlock_fault_byte_two',
                 'status_get_interlock_fault_byte_three'], 'validator': 'interlock_fault_validator'},
        {'tag': 'status_word', 'validator': 'status_word_validator'},
        {'tag': 'status_q_switch_delay', 'validator': 'status_q_switch_delay_validator'},
        {'tag': ['set_flashlamp_frequency', 'status_get_flashlamp_frequency'],
         'validator': 'flashlamp_frequency_validator'}
    ]

    @staticmethod
    def interlock_fault_validator(response):
        """
        Value validator for interlock fault byte one two and three

        Parameters
        ----------
        response: dict

        Returns
        -------
        dict
            response after validation

        """
        value_error_list = list()
        error = False
        for key, value in response['response'].items():
            if value not in ['0', '1']:
                error = True
                value_error_list.append({key: value})
        if error:
            logger.debug('Validator error, value outside valid response limits 0-1: {value} '.format(
                value=str(value_error_list)))
            response['error_message'] = 'Validator error, value outside valid response limits 0-1: ' + \
                                        str(value_error_list)
            response['response'] = None
        return response

    @staticmethod
    def status_word_validator(response):
        """
        Value validator for status word

        Parameters
        ----------
        response: dict

        Returns
        -------
        dict
            response after validation

        """
        value_error_list = list()
        error = False
        for key, value in response['response'].items():
            if key in ['interlocks', 'simmer']:
                if value not in ['0', '1']:
                    error = True
                    value_error_list.append({key: value})
            else:
                if value not in ['0', '2', '4', '6']:
                    error = True
                    value_error_list.append({key: value})
        if error:
            logger.debug('Validator error, value outside valid response limits: {value} '.format(
                value=str(value_error_list)))
            response['error_message'] = 'Validator error, value outside valid response limits: ' + \
                                        str(value_error_list)
            response['response'] = None
        return response

    @staticmethod
    def status_q_switch_delay_validator(response):
        """
        Value validator for q switch delay

        Parameters
        ----------
        response: dict

        Returns
        -------
        dict
          response after validation

        """
        if not is_within_limits(int(response['response']['q_switch_delay']), 0, 999):
            value_error_list = list()
            value_error_list.append({'q_switch_delay': response['response']['q_switch_delay']})
            logger.debug('Validator error, value outside valid response limits 0-999: {value} '.format(
                value=str(value_error_list)))
            response['error_message'] = 'Validator error, value outside valid response limits 0-999: ' + \
                                        str(value_error_list)
            response['response'] = None
        return response

    @staticmethod
    def flashlamp_frequency_validator(response):
        """
        Value validator for flashlamp frequency

        Parameters
        ----------
        response: dict

        Returns
        -------
        dict
          response after validation

        """
        if not is_within_limits(float(response['response']['frequency']), 0, 100.00, flag='floats'):
            value_error_list = list()
            value_error_list.append({'frequency': response['response']['frequency']})
            logger.debug('Validator error, value outside valid response limits 00.000-100.00: {value} '.format(
                value=str(value_error_list)))
            response['error_message'] = 'Validator error, value outside valid response ' \
                                        'limits 00.000-100.00: ' + str(value_error_list)
            response['response'] = None
        return response
