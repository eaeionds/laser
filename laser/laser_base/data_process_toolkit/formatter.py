import logging

logger = logging.getLogger(__name__)


class FormatterBase(object):

    name_assignment_mapper = [
        {'tag': 'status_word', 'assignment_method': 'status_word_assignment'},
        {'tag': 'status_system_state', 'assignment_method': 'status_system_state_assignment'}
    ]

    @staticmethod
    def status_word_assignment(response):
        """
        Name mapper for errors

        Parameters
        ----------
        response: dict

        Returns
        -------
        dict
            response after name assignment

        """

        if response['response']['simmer'] is False:
            response['response']['simmer'] = 'Simmer is not established'
        if response['response']['simmer'] is True:
            response['response']['simmer'] = 'Simmer is established'

        if response['response']['interlocks'] is False:
            response['response']['interlocks'] = 'No interlocks present'
        if response['response']['interlocks'] is True:
            response['response']['interlocks'] = 'Interlocks present'

        if response['response']['flashlamp_sync_mode'] is 0:
            response['response']['flashlamp_sync_mode'] = 'Flashlamp in Internal sync mode and not running'
        if response['response']['flashlamp_sync_mode'] is 2:
            response['response']['flashlamp_sync_mode'] = 'Flashlamp in Internal sync mode and is running'
        if response['response']['flashlamp_sync_mode'] is 4:
            response['response']['flashlamp_sync_mode'] = 'Flashlamp in External sync mode and not running'
        if response['response']['flashlamp_sync_mode'] is 6:
            response['response']['flashlamp_sync_mode'] = 'Flashlamp in External sync mode and is running'

        if response['response']['q_switch_sync_mode'] is 0:
            response['response']['q_switch_sync_mode'] = 'Q-switch in Internal sync mode and off'
        if response['response']['q_switch_sync_mode'] is 1:
            response['response']['q_switch_sync_mode'] = 'Q-switch in Internal sync mode and in single mode'
        if response['response']['q_switch_sync_mode'] is 2:
            response['response']['q_switch_sync_mode'] = 'Q-switch in Internal sync mode and is running'
        if response['response']['q_switch_sync_mode'] is 4:
            response['response']['q_switch_sync_mode'] = 'Q-switch in External sync mode and off'
        if response['response']['q_switch_sync_mode'] is 6:
            response['response']['q_switch_sync_mode'] = 'Q-switch in External sync mode and is running'
        return response

    @staticmethod
    def status_system_state_assignment(response):
        """

        Parameters
        ----------
        response

        Returns
        -------

        """
        if response['system_state'] == 'standby':
            response['system_state'] = 'Laser in idle mode, is not firing and is not simmering'
        elif response['system_state'] == 'simmer':
            response['system_state'] = 'Laser is only simmering'
        elif response['system_state'] == 'fire auto':
            response['system_state'] = 'Flashlamp is firing in Internal sync mode'
        elif response['system_state'] == 'fire ext':
            response['system_state'] = 'Flashlamp is firing in External sync mode'
        elif response['system_state'] == 'fire auto qs':
            response['system_state'] = 'Flashlamp is firing in Internal sync mode and ' \
                                                  'Q- switch is operating in Internal sync mode'
        elif response['system_state'] == 'fire ext qs':
            response['system_state'] = 'Flashlamp is firing in External sync mode and ' \
                                                  'Q- switch is operating in Internal sync mode'
        elif response['system_state'] == 'fire auto qs e':
            response['system_state'] = 'Flashlamp is firing in Internal sync mode and ' \
                                                  'Q- switch is operating in External sync mode'
        elif response['system_state'] == 'fire ext qs e':
            response['system_state'] = 'Flashlamp is firing in External sync mode and ' \
                                                  'Q- switch is operating in External sync mode'

        return response
