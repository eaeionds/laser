import serial
import logging


logger = logging.getLogger(__name__)


class Serial(object):

    def __init__(self, port='COM1', baudrate=9600, parity=None, cmd=None, develop=False, dtr=False):
        self.connection = serial.Serial()
        self.connection.dtr = dtr
        self.connection.port = port
        self.connection.baudrate = baudrate
        self.connection.parity = serial.PARITY_NONE
        self.connection.timeout = 1

    def connect(self):
        """
        Open port. The state of dtr is applied

        Returns
        -------

        """
        self.connection.open()
        logger.debug('Serial [{}] is connected!'.format(self.connection.name))

    def disconnect(self):
        """
        Close port immediately

        Returns
        -------

        """
        self.connection.close()
        logger.debug('Serial [{}] is disconnected!'.format(self.connection.name))

    def is_connected(self):
        """
        Informs if a connection is open or not

        Warnings
        -----
        Deprecated since version 3.0

        Returns
        -------
        bool
           connection.isOpen()

        """
        return self.connection.isOpen()

    def write(self, data):
        self.connection.write('{}\r\n'.format(data).encode())

    def read(self):
        return self.connection.read(17).decode("utf-8")
