import logging
import time

from laser.laser_base.response import Response
from laser.laser_base.serial import Serial

logging.basicConfig(
    format='[%(name)-45s]: %(message)s',
    datefmt='%F %T',
    level=logging.INFO
)
logger = logging.getLogger(__name__)


class Laser(object):

    commands = None
    parser_mappings = None
    formatter = None
    validator = None
    commands_api = None

    is_on = False

    def __init__(self, port='COM1', baudrate=9600, parity=None, cmd=None, develop=False, dtr=False):
        """
        Initiates serial connection for laser

        Parameters
        ----------
        port
        baudrate
        parity
        cmd
        develop

        Returns
        -------

        """
        self.serial = Serial(port, baudrate, parity, cmd, develop, dtr)
        self.response_instance = Response(self.parser_mappings, self.validator, self.formatter)
        self.command_for_request = None

        self.flag = True
        self.response_flag = True
        self.lock = False
        self.is_command_sequence_active = False

    def connect(self):
        """
        Open port. The state of dtr is applied

        Returns
        -------

        """
        self.serial.connect()

    def disconnect(self):
        """
        Close port immediately

        Returns
        -------

        """
        self.serial.disconnect()

    def is_connected(self):
        """
        Informs if a connection is open or not

        Warnings
        -----
        Deprecated since version 3.0

        Returns
        -------
        bool
           connection.isOpen()

        """
        return self.serial.is_connected()

    def write(self, request_dictionary):
        """
        Executes request function to get raw request and writes raw request to serial

        Parameters
        ----------
        request_dictionary: dict

        Returns
        -------

        """
        try:
            if self.is_valid(request_dictionary):
                self.serial.write(self.command_for_request)
                logger.debug(
                    'Sending to LPC: command {command_for_request}'.format(command_for_request=self.command_for_request)
                )
                return None
            else:
                logger.debug('Invalid command {}: Was not sent to serial'.format(self.command_for_request))
                return self.command_for_request
        except Exception as e:
            logger.debug(e)

    def is_valid(self, request_dictionary):
        """
        Checks if user input is valid in order to write command to serial

        Parameters
        ----------
        request_dictionary: dict

        Returns
        -------
        True if command for request is dictionary and False if not

        """
        self.command_for_request = self.commands.execute_request_function(
            request_dictionary['tag'], request_dictionary['data']
        )
        if not isinstance(self.command_for_request, dict):
            logger.debug('Sending to Laser: command {command_for_request}'.format(
                command_for_request=self.command_for_request))
            return True
        else:
            return False

    def read_line(self, request_dictionary=None):
        """
        Reads from serial, decodes data from bytes to string and calls parse_response method to handle response

        Returns
        -------
        Methods
            raw_response_check

        """
        raw_response = None
        try:
            if request_dictionary is not None:
                counter = 1
                while counter > 0:
                    raw_response = self.serial.read()
                    if raw_response:
                        break
                    counter -= 0.01
                    time.sleep(0.01)
            else:
                self.serial.read()
            response = self.response_instance.get(request_dictionary, raw_response)
            logger.debug('Raw response received is: {}'.format(raw_response))
            logger.debug('Parsed response received is: {}'.format(response))
            return response
        except Exception as e:
            logger.debug(e)

    def check_status_system_state(self):
        """
        Checks shutter status and system state in order to get laser's state

        Returns
        -------

        """
        try:
            self.write({'type': 'laser', 'tag': 'status_shutter', 'data': None})
            time.sleep(.2)
            shutter_state = self.read_line({'type': 'laser', 'tag': 'status_shutter', 'data': None})
            self.write({'type': 'laser', 'tag': 'status_system_state', 'data': None})
            status_system_state = self.read_line({'type': 'laser', 'tag': 'status_system_state', 'data': None})
            if shutter_state == 'CLOSE':
                return 'shutter is close, laser state is: {}'.format(status_system_state['system_state'])
            elif shutter_state == 'OPEN' and status_system_state['system_state'] in [
                'Flashlamp is firing in Internal sync mode and Q- switch is operating in Internal sync mode',
                'Flashlamp is firing in External sync mode and Q- switch is operating in Internal sync mode',
                'Flashlamp is firing in Internal sync mode and Q- switch is operating in External sync mode',
                'Flashlamp is firing in External sync mode and Q- switch is operating in External sync mode'
            ]:
                return 'shutter is open and laser is firing, laser status is: {}'.format(
                    status_system_state['system_state'])
            elif shutter_state == 'OPEN' and status_system_state['system_state'] not in [
                'Flashlamp is firing in Internal sync mode and Q- switch is operating in Internal sync mode',
                'Flashlamp is firing in External sync mode and Q- switch is operating in Internal sync mode',
                'Flashlamp is firing in Internal sync mode and Q- switch is operating in External sync mode',
                'Flashlamp is firing in External sync mode and Q- switch is operating in External sync mode'
            ]:
                return 'shutter is open, laser is not firing, laser state is: {}'.format(
                    status_system_state['system_state'])
            else:
                return None
        except Exception as e:
            logger.debug(e)

    @staticmethod
    def validate_status(laser_status_response):
        """
        Check critical variables from Laser status commands

        Parameters
        ----------
        laser_status_response: dict
            parsed and mapped laser response

        Returns
        -------
        tuple
            No errors: True, None
            If errors: False, list of errors

        """
        try:
            critical_variables = list()
            flag = False
            if laser_status_response['id'] in ['status_get_interlock_fault_byte_one',
                                               'status_get_interlock_fault_byte_two',
                                               'status_get_interlock_fault_byte_three']:
                for key, value in laser_status_response['response'].items():
                    if value is True:
                        critical_variables.append({key: value})
                        flag = True
                if flag:
                    return False, critical_variables
                return True, None
            if laser_status_response['id'] == 'status_word':
                if laser_status_response['response']['interlocks'] == 'No interlocks present':
                    return True, None
                else:
                    return False, [{'interlocks': laser_status_response['response']['interlocks']}]
            if laser_status_response['id'] == 'status_get_flashlamp_count_since_reset':
                if laser_status_response['response']['flashlamp_count_since_reset'] <= 50000000:
                    return True, None
                else:
                    return False, [{
                        'flashlamp_count_since_reset': laser_status_response['response'][
                            'flashlamp_count_since_reset'
                        ]
                    }]
            return True, None
        except Exception as e:
            logger.debug(e)
