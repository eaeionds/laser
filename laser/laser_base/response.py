import logging
import re
from laser.tools.mapper import Mapper

logger = logging.getLogger(__name__)


class Response(object):

    def __init__(self, parser_mappings, validator, formatter):
        self.parser_mappings = parser_mappings
        self.validator = validator
        self.formatter = formatter
        self._request_dictionary = None
        self._response = None
        self._raw_response = None

    def get(self, request_dictionary, raw_response=None):
        """
        Chained methods execution for parsing response

        Parameters
        ----------
        request_dictionary: dict
        raw_response: str
            raw data received from laser

        Returns
        -------
        dict
           if parser_matched_response_dict is None: formatted_response
            else: final_response

        """
        if raw_response is None or len(raw_response) < 1:
            return None
        self._request_dictionary = request_dictionary
        self._raw_response = raw_response
        raw_initial = self.raw_response_initial_check()
        if raw_initial is not None:
            return raw_initial
        self._setup_response()
        return self._response

    def raw_response_initial_check(self):
        """
        Method that checks raw response's length and if it is cmd not found

        Parameters
        ----------

        Returns
        -------
        dict
            formatting response

        """
        try:
            # logger.debug('Raw response received is: {}'.format(self._raw_response))
            if len(self._raw_response) == 17 and self._raw_response != '\r\ncmd not found  ':
                self._raw_response = self._raw_response[2:]
                return None
            elif len(self._raw_response) != 17 and self._raw_response != '\r\ncmd not found  ':
                invalid_response = self.initialize_response(self._request_dictionary)
                invalid_response['error_message'] = 'Invalid response length ({}), raw response is: ' \
                                                    '{}'.format(len(self._raw_response), self._raw_response)
                invalid_response['response'] = None
                logger.debug('Invalid response length ({}), raw response is: '
                             '{}'.format(len(self._raw_response), self._raw_response))
                return invalid_response
            elif len(self._raw_response) == 17 and self._raw_response == '\r\ncmd not found  ':
                invalid_response = self.initialize_response(self._request_dictionary)
                invalid_response['error_message'] = 'Invalid command ({request})'.\
                    format(request=self._request_dictionary['tag'])
                invalid_response['response'] = None
                logger.debug(
                    'Invalid command ({request_dictionary})'.format(request_dictionary=self._request_dictionary)
                )
                return invalid_response
        except Exception as e:
            logger.debug(e)

    @staticmethod
    def initialize_response(_request_dictionary):
        response = dict()
        response['type'] = 'laser'
        response['error_message'] = None
        response['id'] = _request_dictionary['tag']
        return response

    def _setup_response(self):
        self.parse()
        if self._response['response'] is not None and self._response['error_message'] is None:
            self._validate()
            if self._response['response'] is not None and self._response['error_message'] is None:
                self._map()
                self._format()
            else:
                return
        else:
            return

    def parse(self):
        """
        Response formatting match based on regex

        Parameters
        ----------

        Returns
        -------

         """
        try:
            regex = self._get_regex(self._request_dictionary, self.parser_mappings)
            self._response = self._get_response(self._request_dictionary, self._raw_response, regex)
        except Exception as exception:
            logger.debug(exception)

    @staticmethod
    def _get_regex(_request_dictionary, _parser_mappings):
        try:
            if _request_dictionary['tag'] == 'set_q_switch_delay':
                return _parser_mappings.parser_mapper_dict['status_q_switch_delay']['regex']
            elif _request_dictionary['tag'] in ['status_get_flashlamp_frequency', 'set_flashlamp_frequency']:
                return _parser_mappings.parser_mapper_dict['flashlamp_frequency']['regex']
            else:
                return _parser_mappings.parser_mapper_dict[_request_dictionary['tag']]['regex']
        except Exception as e:
            logger.debug(e)

    def _get_response(self, _request_dictionary, _raw_response, _regex):
        response = self.initialize_response(_request_dictionary)
        try:

            if re.match(_regex, _raw_response):
                response['response'] = re.match(_regex, _raw_response).groupdict()
            else:
                response['error_message'] = 'Parser error, raw response is: ' + str(_raw_response)
                response['response'] = None
            return response
        except Exception as e:
            logger.debug(e)

    def _validate(self):
        try:
            for validator_dictionary in self.validator.validator_mapper:
                if self._request_dictionary['tag'] in validator_dictionary['tag']:
                    self._response = getattr(self.validator, validator_dictionary['validator'])(self._response)
                    break
        except Exception as e:
            logger.debug(e)

    def _map(self):
        """
        Parameters
        ----------

        Returns
        -------


        """
        try:
            if self._request_dictionary['tag'] == 'set_q_switch_delay':
                mapper = self.parser_mappings.parser_mapper_dict['status_q_switch_delay']['mapping']
            elif self._request_dictionary['tag'] in ['status_get_flashlamp_frequency', 'set_flashlamp_frequency']:
                mapper = self.parser_mappings.parser_mapper_dict['flashlamp_frequency']['mapping']
            else:
                mapper = self.parser_mappings.parser_mapper_dict[self._request_dictionary['tag']]['mapping']
            if mapper is not None and self._response['error_message'] is None:
                self._response['response'] = Mapper().map(self._response['response'], mapper)
        except Exception as e:
            logger.debug(e)

    def _format(self):
        """

        Parameters
        ----------

        Returns
        -------

        """
        try:
            for name_mapping_dictionary in self.formatter.name_assignment_mapper:
                if self._request_dictionary['tag'] == name_mapping_dictionary['tag']:
                    self._response = getattr(
                        self.formatter, name_mapping_dictionary['assignment_method'])(self._response)
                    break
        except Exception as e:
            logger.debug(e)
