class UIControllerBase(object):
    """
    This is responsible for interface rendering.

    Currently argparse interface.
    """

    def __init__(self):
        self._parser = None

    @property
    def parser(self):
        self._set_generic_commands()
        self._set_commands()
        self._set_package_commands()
        return self._parser

    def _set_generic_commands(self):
        """
        Set of commands regarding package version.

        Returns
        -------

        """
        self._parser.add_argument(
            '--com',
            help='select COM port, default: COM1',
            default='COM1'
        )

        self._parser.add_argument(
            '--version',
            help='select laser version to execute commands, default version: 322',
            default='322'
        )

    def _set_commands(self):
        pass

    def _set_package_commands(self):
        """
        Set of commands regarding package and driver information.

        Returns
        -------

        """
        group_package_commands = self._parser.add_argument_group('''Package info''')
        exclusive_group_package_commands = group_package_commands.add_mutually_exclusive_group()
        exclusive_group_package_commands.add_argument(
            '--driver_versions',
            action='store_true',
            help='\t\t\tshow supported versions'
        )
        exclusive_group_package_commands.add_argument(
            '--package_version',
            action='store_true',
            help='\t\t\tshow package version'
        )
        exclusive_group_package_commands.add_argument(
            '--commands',
            action='store_true',
            help='\t\t\tget a list of supported commands (based on laser version)'
        )
