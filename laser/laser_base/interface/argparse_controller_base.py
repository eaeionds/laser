import logging

from laser.laser_main.commands import CommandsVersion322
from laser.laser_main.laser_instance import LaserVersion322

logger = logging.getLogger(__name__)


class ArgparseControllerBase(object):

    def __init__(self, args):
        self.lock = False
        self.package_version = '0.0.0'
        self.command = {'type': 'laser', 'tag': None, 'data': None}
        self.args = args
        self.supported_versions = [  # List of available package firmware versions
            {
                'version_id': '322',
                'laser_instance': LaserVersion322(port=args.com),
                'commands_version_instance': CommandsVersion322()
            }
        ]
        self.selected_version = self._get_selected_version()

    def _get_selected_version(self):
        """
        Get the proper supported_version dictionary, based on selected or default position from args.

        supported_version: [version_id, laser_instance, commands_version_instance]

        Returns
        -------
        """
        for supported_version in self.supported_versions:
            if supported_version['version_id'] == self.args.version:
                return supported_version
        return None

    def run(self):
        """
        Makes all the necessary checks and run requested commands properly.

        Returns
        -------
        """
        if self.selected_version is None:
            logger.error("Laser version is not supported.")
            return
        self._execute_command()
        if not self.lock:
            self._execute_package_command()

    def _execute_command(self):
        pass

    def _execute_package_command(self):
        """
        Check if a package info command is requested and executes it.

        TIP: package commands belong in argparse mutually exclusive group, so only one can be executed at a time.
        Returns
        -------

        """
        if self.args.driver_versions:
            versions = list()
            for item in self.supported_versions:
                versions.append(item['version_id'])
            logger.info(versions)

        if self.args.package_version:
            logger.info(self.package_version)

        if self.args.commands:
            commands = self.selected_version['commands_version_instance'].get_commands()
            result = '\n\n -- Total number of commands supported is: {total}.'.format(total=len(commands))
            for index, command in enumerate(commands):
                result += '\n{index}. {data}'.format(index=index + 1, data=command)
            logger.info(result)
