import copy
import logging

from . import data_types, convert_type


class Mapper(object):

    def __init__(self):
        pass

    def _read_nested_property(self, obj_to_read_from, array_with_names):
        if isinstance(obj_to_read_from, dict):
            return self._read_prop_dict(obj_to_read_from, array_with_names)
        else:
            return self._read_prop(obj_to_read_from, array_with_names)

    def _read_prop_dict(self, instance, name):
        if isinstance(name, list):
            attribute = instance[name[0]]
            name.pop(0)
            if 0 == len(name):
                return attribute
            return self._read_nested_property(attribute, name)
        else:
            if name in instance:
                return instance[name]
            else:
                return None

    def _read_prop(self, instance, name):
        if isinstance(name, list):
            if isinstance(name[0], int):
                attribute = instance[name[0]] if len(instance) > name[0] else None
            else:
                attribute = getattr(instance, name[0])
            name.pop(0)
            if 0 == len(name):
                return attribute
            return self._read_nested_property(attribute, name)
        else:
            if hasattr(instance, 'attributes'):
                if name in instance.attributes:
                    return getattr(instance, name)
                else:
                    return None
            else:
                if name in instance:
                    return getattr(instance, name)
                else:
                    return None

    def filter_data(self, data_object, selected_field, mapper):
        try:
            value = self._read_nested_property(data_object, selected_field)
        except AttributeError:
            logging.info("Can't get {selected_field} using {mapper} from {data_object}".format(
                selected_field=selected_field,
                mapper=mapper,
                data_object=data_object))
            value = None
        value = convert_type.to_general(value)

        if value is not None and '' != value:
            if 'callback' in mapper and mapper['callback']:
                value = mapper['callback'](value)
            else:
                if data_types.BOOL == mapper['type']:
                    value = convert_type.to_bool(value)
                elif data_types.STRING == mapper['type']:
                    value = convert_type.to_string(value)
                elif data_types.FLOAT == mapper['type']:
                    value = convert_type.to_float(value)
                elif data_types.INT == mapper['type']:
                    value = convert_type.to_int(value)
        return value

    def map(self, data_object, mapper):
        formatted_data_object = dict()
        mapper_array = sorted(mapper, key=lambda k: k['type'])

        for i, mapper_attribute in enumerate(mapper_array):
            mapper_instance = copy.deepcopy(mapper_attribute)
            if 'value' in mapper_instance:
                value = mapper_instance['value']
            else:
                field = mapper_instance['code']
                value = self.filter_data(data_object, field, mapper_instance)

            if 'default' in mapper_instance:
                if value is None or '' == value:
                    value = mapper_instance['default']

            if value is not None and '' != value:
                formatted_data_object.update({mapper_instance['code']: value})
        return formatted_data_object
