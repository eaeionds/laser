import logging

logger = logging.getLogger(__name__)


def is_int(arg):
    """
    Check if the argument is an integer

    Parameters
    ----------
    arg : argument

    Returns
    -------
    bool
    """
    return isinstance(arg, int)


def is_boolean(arg):
    """
    Check if the argument is a boolean

    Parameters
    ----------
    arg : argument

    Returns
    -------
    bool
    """
    return isinstance(arg, bool)


def is_float(arg):
    """
    Check if the argument is an integer

    Parameters
    ----------
    arg : argument

    Returns
    -------
    bool
    """
    return isinstance(arg, float)


def _custom_range(x, y, step=0.01):
    """
    Custom function which replaces build in limited range function

    Parameters
    ----------
    x :
        initial boundary
    y :
        final boundary
    step :
        step value

    Notes
    -----
    This one is working with floats too

    Values are rounded to model_2 decimal digits. -- Default

    Returns
    -------
    list of values between x and y with step [step]
    """
    while x <= y+0.01:
        yield round(x, 2)
        x += step


def _format_numbers(args, fix=2, flag=None):
    """
    Private function which accepts a list of arguments and format them to float.

    Parameters
    ----------
    args : list of arguments
    fix : number of decimal digits
    flag : string

    Notes
    -----
    It checks if each one of them if it is not None and (int or float)

    If any item is int, it converts it to float with model_2 decimal digits -- Default

    Returns
    -------
    formatted list of arguments -- same amount of arguments as received
    """
    result = list()
    try:
        for arg in args:
            if flag is None:
                if is_int(arg):
                    result.append(round(float(arg), fix))
                else:
                    raise TypeError('Argument must be integer')
            elif flag is not None:
                if is_float(arg) or is_int(arg):
                    result.append(round(float(arg), fix))
                else:
                    raise TypeError('Argument must be float or integer')
        return result
    except TypeError as t:
        logger.debug(t)
    except Exception as e:
        logger.debug(e)


def is_within_limits(arg, limit_from=None, limit_to=None, flag=None):
    """
    Check if limit restrictions exist and are met

    Parameters
    ----------
    arg : int,float
    limit_from : int,float
    limit_to : int,float
    flag :

    Warnings
    -----
    If flag is None arg (user inputs) can only be integers

    Returns
    -------
    bool
    """
    try:
        if arg is not None and limit_from is not None and limit_to is not None:
            arg, limit_from, limit_to = _format_numbers([arg, limit_from, limit_to], flag=flag)
            return True if arg in _custom_range(limit_from, limit_to) else False
        elif arg is not None and limit_from is not None and limit_to is None:
            arg, limit_from = _format_numbers([arg, limit_from], flag=flag)
            return True if arg >= limit_from else False
        elif arg is not None and limit_from is None and limit_to is not None:
            arg, limit_to = _format_numbers([arg, limit_to], flag=flag)
            return True if arg <= limit_to else False
        elif arg is not None and limit_from is None and limit_to is None:
            _format_numbers([arg])
            return True
        else:
            raise ValueError
    except ValueError as t:
        logger.debug(t)
