import logging

from laser.laser_base.commands import CommandsBase

logger = logging.getLogger(__name__)


class CommandsVersion322(CommandsBase):

    @staticmethod
    def get_commands():
        """
        Get all laser's available commands

        Returns
        -------
        commands
        """
        commands = [func for func in dir(CommandsVersion322) if callable(getattr(CommandsVersion322, func)) and
                    not func.startswith('__') and not func.startswith('get_commands')
                    and not func.startswith('execute_request_function')
                    and not func.startswith('invalid_input_setup')]
        return commands
