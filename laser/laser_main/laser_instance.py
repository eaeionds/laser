import logging

from laser.laser_main.commands import CommandsVersion322
from laser.laser_main.commands_api import commands_api_version_322
from laser.laser_main.data_process_toolkit.data_validator import ValidatorVersion322
from laser.laser_main.data_process_toolkit.formatter import FormatterVersion322
from laser.laser_main.data_process_toolkit.parser_mappings import ParserMapperVersion322
from laser.laser_base.laser_instance import Laser

logger = logging.getLogger(__name__)


class LaserVersion322(Laser):

    commands = CommandsVersion322
    parser_mappings = ParserMapperVersion322
    validator = ValidatorVersion322
    formatter = FormatterVersion322
    commands_api = commands_api_version_322

    def __init__(self, port='COM1', baudrate=9600, parity=None, cmd=None, develop=False, dtr=False):
        """
        Initiates serial connection for LPC

        Parameters
        ----------
        port
        baudrate
        parity
        cmd
        develop
        """
        super().__init__(port, baudrate, parity, cmd, develop, dtr)
