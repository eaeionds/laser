import argparse

from laser.laser_base.interface.ui_controller_base import UIControllerBase


class UIController(UIControllerBase):
    """
    This is responsible for interface rendering.

    Currently argparse interface.
    """

    def __init__(self):
        super().__init__()
        self._parser = argparse.ArgumentParser(
            description="""Laser.\t\tPython library for communication with laser.
                        Supports: serial communication, commands and validation,
                        as well as response parsing and mapping""",
            formatter_class=argparse.RawTextHelpFormatter,
            epilog='Company: Raymetrics'
        )

    def _set_commands(self):
        """
        Set of commands regarding component firmware.

        Returns
        -------

        """
        group_laser_commands = self._parser.add_argument_group('''laser commands''')
        commands_group = group_laser_commands.add_mutually_exclusive_group()

        commands_group.add_argument(
            '--get_state',
            action='store_true',
            help='\t\t\tQueries the state of the laser.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_status_system_state',
            action='store_true',
            help='\t\t\tQueries the system state.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_fault_1',
            action='store_true',
            help='\t\t\tQueries Interlock Fault Byte 1.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_fault_2',
            action='store_true',
            help='\t\t\tQueries Interlock Fault Byte 2.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_fault_3',
            action='store_true',
            help='\t\t\tQueries Interlock Fault Byte 3.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_coolant_flow',
            action='store_true',
            help='\t\t\tQueries the coolant flow rate in liters per minute (lpm).\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_temperatures',
            action='store_true',
            help='\t\t\tQueries all three temperatures at once.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_coolant_level',
            action='store_true',
            help='\t\t\tQueries the coolant level status.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_flashlamp_total',
            action='store_true',
            help='\t\t\tQueries total number of flashlamp firings since shipment.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_flashlamp_since_reset',
            action='store_true',
            help='\t\t\tQueries number of flashlamp firings since count reset.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_q_switch_delay',
            action='store_true',
            help='\t\t\tQueries the Q-switch delay setting.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_flashlamp_frequency',
            action='store_true',
            help='\t\t\tQueries the flashlamp firing frequency.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--get_status_shutter',
            action='store_true',
            help='\t\t\tQueries the shutter status.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_flashlamp_simmer',
            action='store_true',
            help='\t\t\tSets flashlamp into Simmer mode.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_q_switch_delay',
            help='\t\t\tSets the Q-switch delay.'
            '\n\t\t\texample: --set_q_switch_delay <q_switch_delay>'
            '\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_flashlamp_internal',
            action='store_true',
            help='\t\t\tSets flashlamp into Internal sync mode to start flashing.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_start_q_switch',
            action='store_true',
            help='\t\t\tStarts Q-Switching.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_open_shutter',
            action='store_true',
            help='\t\t\tOpens shutter.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_disable_q_switch',
            action='store_true',
            help='\t\t\tDisables Q-Switch.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_stop_flashlamp',
            action='store_true',
            help='\t\t\tStops flashlamp from flashing and turns off the simmer.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_close_shutter',
            action='store_true',
            help='\t\t\tCloses shutter.\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
        commands_group.add_argument(
            '--set_flashlamp_frequency',
            help='\t\t\tSets the flashlamp firing frequency.'
            '\n\t\t\texample: --set_flashlamp_frequency <flashlamp_frequency>'
            '\n\t\t\tSupported versions: [322]\n\n',
            default=False
        )
