import logging

from laser.laser_base.interface.argparse_controller_base import ArgparseControllerBase
from laser.laser_main.interface.component_controller import ComponentController

logger = logging.getLogger(__name__)


class ArgparseController(ArgparseControllerBase):

    def __init__(self, args):
        super().__init__(args)

    def _execute_command(self):
        """
        Check if a component command is requested and executes it using CommandsController.

        Returns
        -------

        """
        self._set_up()
        laser_instance = self.selected_version['laser_instance']
        if self.command['tag'] is not None:
            self.lock = True
            commands_controller = ComponentController(laser_instance)
            commands_controller.run(self.command)

    def _set_up(self):
        """
        Set up command based on args attributes.

        TIP: component commands belong in argparse mutually exclusive group, so only one can be executed at a time.

        Returns
        -------

        """
        if self.args.get_state:
            self.command['tag'] = 'status_word'
        if self.args.get_status_system_state:
            self.command['tag'] = 'status_system_state'
        if self.args.get_fault_1:
            self.command['tag'] = 'status_get_interlock_fault_byte_one'
        if self.args.get_fault_2:
            self.command['tag'] = 'status_get_interlock_fault_byte_two'
        if self.args.get_fault_3:
            self.command['tag'] = 'status_get_interlock_fault_byte_three'
        if self.args.get_coolant_flow:
            self.command['tag'] = 'status_get_coolant_flow_level_lpm'
        if self.args.get_temperatures:
            self.command['tag'] = 'status_get_all_three_temperatures'
        if self.args.get_coolant_level:
            self.command['tag'] = 'status_get_coolant_level'
        if self.args.get_flashlamp_total:
            self.command['tag'] = 'status_get_flashlamp_count'
        if self.args.get_flashlamp_since_reset:
            self.command['tag'] = 'status_get_flashlamp_count_since_reset'
        if self.args.get_q_switch_delay:
            self.command['tag'] = 'status_q_switch_delay'
        if self.args.get_flashlamp_frequency:
            self.command['tag'] = 'status_get_flashlamp_frequency'
        if self.args.get_status_shutter:
            self.command['tag'] = 'status_shutter'

        if self.args.set_flashlamp_simmer:
            self.command['tag'] = 'start_set_flashlamp_simmer'

        if self.args.set_flashlamp_internal:
            self.command['tag'] = 'start_set_flashlamp_fire'
        if self.args.set_start_q_switch:
            self.command['tag'] = 'start_q_switch'
        if self.args.set_open_shutter:
            self.command['tag'] = 'start_open_shutter'
        if self.args.set_disable_q_switch:
            self.command['tag'] = 'stop_q_switch'
        if self.args.set_stop_flashlamp:
            self.command['tag'] = 'stop_flash'
        if self.args.set_close_shutter:
            self.command['tag'] = 'stop_close_shutter'

        if self.args.set_flashlamp_frequency:
            flashlamp_frequency = self.args.set_flashlamp_frequency
            self.command['tag'] = 'set_flashlamp_frequency'
            self.command['data'] = int(flashlamp_frequency)
        if self.args.set_q_switch_delay:
            q_switch_delay = self.args.set_q_switch_delay
            self.command['tag'] = 'set_q_switch_delay'
            self.command['data'] = int(q_switch_delay)
