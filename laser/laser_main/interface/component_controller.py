import logging
import time

logger = logging.getLogger(__name__)


class ComponentController(object):
    """
    This is responsible for the laser command execution and response verification.
    """

    def __init__(self, laser_instance=None):
        self.command = None
        self.laser_instance = laser_instance

    def run(self, command):
        """
        Executes a single command in laser.

        Connects to laser.
        Writes the given command.
        Waiting till receiving the expected answer.
        Disconnects from laser.

        TIP: Based on given command, selects the proper <verify_response> function, in order to stop the iteration.

        Parameters
        ----------
        command

        Returns
        -------

        """
        self.command = command
        logger.info("Command: {}".format(command))

        self.laser_instance.connect()
        if self.laser_instance.write(command) is None:
            counter = 20  # Max number of seconds for iteration
            while counter > 0:
                response = self.laser_instance.read_line(command)
                if response is not None and len(response) > 0:
                    logger.info('Response: {}'.format(response))
                    break
                counter -= 0.001
                time.sleep(0.001)
        self.laser_instance.disconnect()
