import logging

from laser.laser_base.data_process_toolkit.parser_mappings import ParserMappingsBase
from laser.laser_main.data_process_toolkit.response_mappings import response_mappings_version_322

logger = logging.getLogger(__name__)


class ParserMapperVersion322(ParserMappingsBase):
    mappings = response_mappings_version_322
