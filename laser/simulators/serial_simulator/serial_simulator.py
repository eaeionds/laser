#############################################
# Simulator for serial.
#
# It is used only for test purposes.
#
# Each time it must be configured with proper
# drivers depending on component version we
# want to test.
#############################################
import time
import logging

from laser.simulators.serial_simulator.drivers.version_322 import normal

logging.basicConfig(
    format="%(asctime)-8s %(name)-40s (%(lineno)d): %(message)s",
    datefmt='%F %T',
    level=logging.INFO)
logger = logging.getLogger(__name__)


class SerialSimulator(object):

    test_cases = normal

    def __init__(self):
        self.is_connected = False
        self.response_list = None
        self._generator_instance = self._generator()

    def connect(self):
        self.is_connected = True
        logger.debug('Connected to serial simulation')

    def disconnect(self):
        self.is_connected = False
        logger.debug('Disconnected from serial simulation')

    def read(self):
        if not self.is_connected:
            raise Exception('Connection error')
        try:
            return next(self._generator_instance)
        except Exception:
            self.response_list = None
            return None

    def write(self, command):
        if not self.is_connected:
            raise Exception('Connection error')
        if self.response_list is not None:
            return None
        for test_case in SerialSimulator.test_cases:
            if test_case['command'] == command:
                self.response_list = test_case['read_sequence']
                break

    def _generator(self):
        """
        A generator function, which returns specific responses after the predefined delay.

        :return:
        """
        while True:
            counter = 0
            max_repetitions = len(self.response_list) if self.response_list is not None else 0
            while counter < max_repetitions:
                _data = self.response_list[counter]
                time.sleep(_data['delay'])
                yield _data['response']
                counter += 1
            self.response_list = None
            yield '/n'
