errors = []

normal = [
    {
        'case_id': 1,
        'command': 'WOR',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nI 0 F 0 S 0 Q 0'
            }
        ]
    },
    {
        'case_id': 2,
        'command': 'ST',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\ncmd not found  '
            }
        ]
    },
    {
        'case_id': 3,
        'command': 'IF1',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nIF1 00 00 00 00'
            }
        ]
    },
    {
        'case_id': 4,
        'command': 'IF2',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nIF2 00 00 00 00'
            }
        ]
    },
    {
        'case_id': 5,
        'command': 'IF3',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nIF3 00 00 00 00'
            }
        ]
    },
    {
        'case_id': 6,
        'command': 'FLOW',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nFLOW 2.450 lpm '
            }
        ]
    },
    {
        'case_id': 7,
        'command': 'T3',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nT3 372 553 561 '
            }
        ]
    },
    {
        'case_id': 8,
        'command': 'LEV',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nLEV  OK        '
            }
        ]
    },
    {
        'case_id': 9,
        'command': 'F',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\ncu LP0020000000'
            }
        ]
    },
    {
        'case_id': 10,
        'command': 'UF',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\ncu LP0020000000'
            }
        ]
    },
    {
        'case_id': 11,
        'command': 'W',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\ndelay 100 uS   '
            }
        ]
    },
    {
        'case_id': 12,
        'command': 'SHC',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nshc: CLOSE     '
            }
        ]
    },
    {
        'case_id': 13,
        'command': 'D',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nfreq.  020.00Hz'
            }
        ]
    },
    {
        'case_id': 14,
        'command': 'M',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nsimmer         '
            }
        ]
    },
    {
        'case_id': 15,
        'command': 'A',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nfire auto      '
            }
        ]
    },
    {
        'case_id': 16,
        'command': 'CC',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nfire auto qs   '
            }
        ]
    },
    {
        'case_id': 17,
        'command': 'SHC1',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nshc: OPEN      '
            }
        ]
    },
    {
        'case_id': 18,
        'command': 'CS',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nfire auto      '
            }
        ]
    },
    {
        'case_id': 19,
        'command': 'S',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nstandby        '
            }
        ]
    },
    {
        'case_id': 20,
        'command': 'SHC0',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nshc: CLOSE     '
            }
        ]
    },
    {
        'case_id': 21,
        'command': 'W1',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\ndelay 001 uS   '
            }
        ]
    },
    {
        'case_id': 22,
        'command': 'D2',
        'read_sequence': [
            {
                'delay': .1,
                'response': '\r\nfreq.  000.02Hz'
            }
        ]
    },
]

all_cases = normal + errors
