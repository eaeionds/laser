# Package info
This is a wrapper which allows communication between a python program and a laser through RS232,
using pyserial library.

## Generate package

When generating new package version, run:
`scripts\generate.bat`

This will create/update directories: _build, dist_ and _<package_name>.egg-info</package_name>_.
Only _dist_ directory is uploaded in Git Lab and includes the final package.

## Install package through pip

Activate the proper virtual environment.

`workon virtual_environment_name`

`pip install path_to_package/package_name`

## Versioning

The versioning strategy is as follows:

*   **0.0.0** Initial version
*   **0.0.1** Version after one or more bug fixes
*   **0.0.2** Version after one or more bug fixes
*   **0.1.2** Version after one or more feature improvement
*   **1.0.0** Version after adding one or more new features

For each new version, a VERSION-"version_number"_DESCRIPTION.md will be created,
with all the updates and changes made.
This will be located in the _dist_ directory.

## Package dependencies

Each package/sub-package may need to use some other python libraries.
In order to achieve this, you must declare these dependencies in setup.py
If the package has dependencies, when you install it in pip,
you will install as well, all the package's dependencies.

* python_version=="3.6"
* pyserial==3.4
* Sphinx>=1.6.6
* argparse==1.4.0
* pytest==3.3.2
* pytest-cov==2.5.1


## Generate Docs

Create directory: **docs** inside the main package directory. Navigate to docs and Run:

*   `sphinx-quickstart`
    Setup Sphinx procedure This is the initial command for sphinx.
    Follow the prompt to set it up properly
    Procedure details:
    Root path for the documentation: 
*   Go to docs/source/conf.py and insert the following at the top of the file:
    `import os`
    `import sys`
    `sys.path.append(os.path.abspath('../..'))`
    `sys.path.append(os.path.abspath('..'))`
    After that, navigate to **docs** and execute: `sphinx-apidoc -f -o source/ ../mypackage/`
    This generates documentation from docstrings automatically.
    Remove all rst test files.
    TIP: mypackage is under main package directory

*   In **docs** execute command: `make html`

## Testing

In the wrapper package, there is a directory named: _tests_, which includes all the test files and reports from coverage.

*   run all project's tests
    `pytest`
*   For testing specific files type pytest and the path to the file you want to test, for example:
    `pytest tests/model_2/unit_tests`
*   generate html output for coverage data
    `pytest --cov-report html:tests/reports --cov`
    (after **html:** we give the folder that will contain the tests coverage,
    not necessary to have been created previously)
      or `py.test --cov=tests/ --cov-report=html`

