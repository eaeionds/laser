# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [0.0.0] - 2018-03-09
### Added
- Documentation through Sphinx
- Gitlab CI through local runner, using docker python image - Unit testing on the fly
- Test suit with unit and integration tests, using pytest framework
- Parsing, mapping and validation of laser response
- Laser Response with access to both, raw and formatted data
- Currently supported Laser:
    * Model: Quantel

[0.0.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v0.0.0
